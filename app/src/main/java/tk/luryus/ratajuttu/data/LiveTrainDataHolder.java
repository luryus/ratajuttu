package tk.luryus.ratajuttu.data;

/**
 * Created by Lauri Koskela on 12/09/15.
 */
public class LiveTrainDataHolder {

    private String mOriginStation;
    private String mDestinationStation;
    private String mTrainTitle;
    private String mScheduledOriginDeparture;
    private String mScheduledDestinationArrival;

    public LiveTrainDataHolder(String originStation, String destinationStation, String trainTitle,
            String scheduledOriginDeparture, String scheduledDestinationArrival) {
        mOriginStation = originStation;
        mDestinationStation = destinationStation;
        mTrainTitle = trainTitle;
        mScheduledOriginDeparture = scheduledOriginDeparture;
        mScheduledDestinationArrival = scheduledDestinationArrival;
    }

    public String getOriginStation() {
        return mOriginStation;
    }

    public String getDestinationStation() {
        return mDestinationStation;
    }

    public String getTrainTitle() {
        return mTrainTitle;
    }

    public String getScheduledOriginDeparture() {
        return mScheduledOriginDeparture;
    }

    public String getScheduledDestinationArrival() {
        return mScheduledDestinationArrival;
    }
}
