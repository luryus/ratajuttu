package tk.luryus.ratajuttu.data;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import rx.Observable;
import rx.android.app.AppObservable;
import rx.schedulers.Schedulers;
import tk.luryus.ratajuttu.provider.StationUtils;
import tk.luryus.ratajuttu.rata.LiveTrainListRata;
import tk.luryus.ratajuttu.rata.entity.Train;
import tk.luryus.ratajuttu.ui.adapter.LiveTrainListAdapter;
import tk.luryus.ratajuttu.ui.fragment.LiveTrainListFragment;
import tk.luryus.ratajuttu.util.DateUtils;

/**
 *
 *
 * Created by Lauri Koskela on 11/09/15.
 */
public class LiveTrainRefreshManager {

    private static final String TAG = "LiveTrainRefreshManager";

    private LiveTrainListAdapter mAdapter;
    private LiveTrainListFragment mFragment;

    public LiveTrainRefreshManager(LiveTrainListFragment fragment, LiveTrainListAdapter adapter) {
        mFragment = fragment;
        mAdapter = adapter;
    }

    public void refreshLiveTrains() {
        mFragment.refreshStarted();

        mAdapter.getContent().clear();

        Log.d(TAG, "Firing up retrofit request now");

        Observable<LiveTrainDataHolder> obs = LiveTrainListRata.getApiService().getLiveTrains()
                // retrofit returns a list, sort and filter it
                .doOnNext(trainList -> {
                    Log.d(TAG, "Got response from retrofit");
                    filterTrains(trainList);
                    sortTrains(trainList);
                })
                // list to basic observable
                .flatMap(Observable::from)
                // train objects to train data holders
                .map(this::buildLiveTrainDataHolder)
                // add data holders to the adapter content
                .doOnNext(holder -> mAdapter.getContent().add(holder))
                // do this all on a background thread
                .subscribeOn(Schedulers.computation());

        AppObservable
                .bindSupportFragment(mFragment, obs)
                .subscribe(
                        holder -> {
                        },
                        throwable -> {
                            Toast.makeText(mFragment.getActivity(), "Error fetching trains", Toast.LENGTH_SHORT)
                                 .show();
                            Log.e(TAG, "Error fetching trains", throwable);
                            mFragment.refreshCompleted();
                        },
                        () -> {
                            Log.d(TAG, "Notifying adapter about new data now");
                            mAdapter.notifyDataSetChanged();
                            mFragment.refreshCompleted();
                            Log.d(TAG, "Refresh completed");
                        }
                );
    }

    private void filterTrains(List<Train> trainList) {
        Log.d(TAG, "Starting to filter the trains");
        ListIterator<Train> i = trainList.listIterator();
        while (i.hasNext()) {
            Train t = i.next();
            if (!t.getTrainCategory().equals(Train.TRAIN_CATEGORY_COMMUTER)
                    && !t.getTrainCategory().equals(Train.TRAIN_CATEGORY_LONG_DISTANCE)) {
                i.remove();
            }
        }
        Log.d(TAG, "Done filtering trains.");
    }

    private void sortTrains(List<Train> trainList) {
        Log.d(TAG, "Starting to sort the live trains");

        String[] cats = {
                Train.TRAIN_CATEGORY_LONG_DISTANCE,
                Train.TRAIN_CATEGORY_COMMUTER,
                Train.TRAIN_CATEGORY_CARGO,
                Train.TRAIN_CATEGORY_LOCOMOTIVE,
                Train.TRAIN_CATEGORY_SHUNTING,
                Train.TRAIN_CATEGORY_ON_TRACK_MACHINES
        };
        List<String> categories = Arrays.asList(cats);

        Collections.sort(trainList, (trainA, trainB) -> {
            int categoryA = categories.indexOf(trainA.getTrainCategory());
            int categoryB = categories.indexOf(trainB.getTrainCategory());

            if (categoryA == -1) {
                categoryA = categories.size();
                categories.add(trainA.getTrainCategory());
                categoryB = categories.indexOf(trainB.getTrainCategory());
            }
            if (categoryB == -1) {
                categoryB = categories.size();
                categories.add(trainB.getTrainCategory());
            }

            if (categoryA != categoryB) {
                return ((Integer) categoryA).compareTo(categoryB);
            } else {
                // the trains are of the same category, compare their titles
                return trainA.getTrainTitle().compareTo(trainB.getTrainTitle());
            }
        });
        Log.d(TAG, "Done sorting trains");
    }

    private LiveTrainDataHolder buildLiveTrainDataHolder(Train train) {
        Context ctx = mFragment.getActivity().getApplicationContext();

        return new LiveTrainDataHolder(
                StationUtils.getStationNameWithCode(train.getOriginShortCode(), ctx),
                StationUtils.getStationNameWithCode(train.getDestinationShortCode(), ctx),
                train.getTrainTitle(),
                DateUtils.getTrainListFormattedDate(train.getScheduledOriginDeparture(), ctx),
                DateUtils.getTrainListFormattedDate(train.getScheduledDestinationArrival(), ctx)
        );
    }

    public interface LiveTrainRefreshCallbacks {
        void refreshStarted();
        void refreshCompleted();
    }
}
