package tk.luryus.ratajuttu.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.ArraySet;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tk.luryus.ratajuttu.rata.Rata;
import tk.luryus.ratajuttu.rata.entity.Station;
import tk.luryus.ratajuttu.provider.RataContract.StationEntry;

/**
 * Created by luryus on 26/06/15.
 */
public class FetchStationsTask extends AsyncTask<Boolean, Void, Void> {

    private static final String LOG_TAG = "FetchStationsTask";

    private final Context mContext;

    public FetchStationsTask(Context context) {
        this.mContext = context;
    }

    /**
     *
     * @param params Pass in true if forced update should be performed
     * @return void
     */
    @Override
    protected Void doInBackground(Boolean... params) {

        // get the count of existing stations in db
        Cursor countCursor = mContext.getContentResolver().query(
                StationEntry.CONTENT_URI,
                new String[] {StationEntry._COUNT},
                null, null, null);

        // params[0] indicates whether a forced update should be done
        if (params[0] || countCursor == null || !countCursor.moveToFirst()) {
            // we do not have any station records, fetch new ones

            Log.d(LOG_TAG, "Starting to fetch stations");

            // todo add error handling
            List<Station> stations = Rata.getApiService().getStations();

            ArraySet<String> trainStationCodes = null;
            try {
                trainStationCodes = TrainStationManager.fetchTrainStations();
            } catch (IOException e) {
                // todo error handling
                e.printStackTrace();
            }

            int inserted = 0;
            if (stations.size() > 0) {
                ContentValues[] cvs = buildContentValues(stations, trainStationCodes);

                // The bulk insert replaces on conflict, so no need to check for need to update
                inserted = mContext.getContentResolver().bulkInsert(StationEntry.CONTENT_URI, cvs);
            }

            Log.d(LOG_TAG, "Done fetching stations, inserted " + inserted + " rows");
        } else {
            Log.d(LOG_TAG, "Not fetching new stations");
        }

        if (countCursor != null) {
            countCursor.close();
        }

        return null;
    }

    private ContentValues[] buildContentValues(List<Station> stations, ArraySet<String> trainStationCodes) {
        ArrayList<ContentValues> cvs = new ArrayList<>(stations.size());

        for (Station s : stations) {
            ContentValues cv = new ContentValues();

            cv.put(StationEntry.STATION_SHORT_CODE, s.getStationShortCode());
            cv.put(StationEntry.COUNTRY_CODE, s.getCountryCode());
            cv.put(StationEntry.STATION_NAME, s.getStationName());
            cv.put(StationEntry.STATION_UIC_CODE, s.getStationUICCode());
            cv.put(StationEntry.LATITUDE, s.getLatitude());
            cv.put(StationEntry.LONGITUDE, s.getLongitude());

            // the station codes in the json _should_ be always uppercase
            if (trainStationCodes != null && trainStationCodes.contains(s.getStationShortCode().toUpperCase())) {
                cv.put(StationEntry.IS_TRAIN_STATION, 1);
            } else {
                cv.put(StationEntry.IS_TRAIN_STATION, 0);
            }

            cvs.add(cv);
        }

        return cvs.toArray(new ContentValues[cvs.size()]);
    }
}
