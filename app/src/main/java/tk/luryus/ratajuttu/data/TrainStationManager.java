package tk.luryus.ratajuttu.data;

import android.util.ArraySet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * A helper class for loading the VR train station list from luryus.github.io
 *
 * Created by Lauri Koskela on 29/08/15.
 */
public class TrainStationManager {

    private static final String JSON_URL = "https://luryus.github.io/vr-stations/stations.json";

    public static ArraySet<String> fetchTrainStations() throws IOException {
        // construct the http request for the json, and execute
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(JSON_URL).build();
        Response resp = client.newCall(request).execute();

        // make a string array of the returned codes
        return parseResponse(resp);
    }

    private static ArraySet<String> parseResponse(Response resp) throws IOException {
        String jsonArray = resp.body().string();
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(jsonArray, new TypeToken<ArraySet<String>>(){}.getType());
    }
}
