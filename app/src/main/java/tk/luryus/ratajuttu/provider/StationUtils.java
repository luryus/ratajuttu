package tk.luryus.ratajuttu.provider;

import android.content.Context;
import android.database.Cursor;

import tk.luryus.ratajuttu.provider.RataContract.StationEntry;

/**
 * Created by Lauri Koskela on 12/09/15.
 */
public class StationUtils {

    public static String getStationNameWithCode(String shortCode, Context context) {
        Cursor cursor = context.getContentResolver().query(
                StationEntry.buildStationUriWithShortCode(shortCode),
                new String[]{StationEntry._ID, StationEntry.STATION_NAME},
                null,
                null,
                null
        );

        String name = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                name = cursor.getString(1);
            }
            cursor.close();
        }

        return name;
    }
}
