package tk.luryus.ratajuttu.provider;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by luryus on 26/06/15.
 * <p>
 * Defines tables for Rata db.
 */
public class RataContract {

    /**
     * The unique name of the content provider.
     */
    public static final String CONTENT_AUTHORITY = "tk.luryus.ratajuttu.app";

    /**
     * The base content provider uri, generated with the content authority.
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /**
     * Possible paths to use with the content provider.
     */
    public static final String PATH_STATION = "station";
    public static final String PATH_TRAIN = "train";
    public static final String PATH_TIMETABLE_ROW = "timetablerow";

    public static final class StationEntry implements BaseColumns {

        /**
         * The content provider Uri to access station table.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_STATION).build();

        /**
         * Content type definitions for accessing (making cursors) with the content provider
         */
        public static final String CONTENT_DIR_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" +
                        PATH_STATION;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" +
                        PATH_STATION;

        /**
         * Database table name
         */
        public static final String TABLE_NAME = "Station";

        /**
         * Column names
         * <p>
         * Note: _ID comes from BaseColumns, no need for a definition here
         */
        public static final String STATION_SHORT_CODE = "station_short_code";
        public static final String COUNTRY_CODE = "country_code";
        public static final String STATION_NAME = "station_name";
        public static final String STATION_UIC_CODE = "station_uic_code";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String IS_TRAIN_STATION = "is_train_station";

        /**
         * This is not a real column, but a field that is used when fetching station list with favorites.
         */
        public static final String IS_FAVORITE = "is_favorite";


        public static Uri getStationsWithFavoritesUri() {
            return CONTENT_URI.buildUpon().appendPath("favorites").build();
        }

        /**
         * Gets a station short code out of a content uri
         *
         * @param uri The uri to get the code from
         *
         * @return The station short code
         */
        public static String getStationShortCodeFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        /**
         * Make an uri with specified id
         *
         * @param id The id to use
         *
         * @return The uri with the id
         */
        public static Uri buildStationUriWithId(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        /**
         * Make an uri with specified short code
         *
         * @param shortCode The short code to use
         *
         * @return The uri with the short code
         */
        public static Uri buildStationUriWithShortCode(String shortCode) {
            return CONTENT_URI.buildUpon().appendPath(shortCode).build();
        }
    }
}
