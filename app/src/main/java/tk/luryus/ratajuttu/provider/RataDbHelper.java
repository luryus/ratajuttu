package tk.luryus.ratajuttu.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import tk.luryus.ratajuttu.provider.RataContract.StationEntry;

/**
 * A database helper. Handles creation and upgrading of
 * our database.
 *
 * Created by luryus on 26/06/15.
 */
public class RataDbHelper extends SQLiteOpenHelper {

    /**
     * The version of the database schema.
     * If changes are made, version must be bumped and migration code added.
     */
    public static final int DATABASE_VERSION = 4;

    /**
     * The database name.
     */
    public static final String DATABASE_NAME = "rata.db";

    public RataDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Creates tables and stuff. Only called when first creating the database.
     *
     * @param db The newly created database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        // make a query for creating the table
        final String SQL_CREATE_STATION_TABLE =
                "CREATE TABLE " + StationEntry.TABLE_NAME + "(" +
                        StationEntry._ID + " INTEGER PRIMARY KEY, " +
                        StationEntry._COUNT + " INTEGER, " +
                        StationEntry.STATION_SHORT_CODE + " TEXT UNIQUE NOT NULL, " +
                        StationEntry.COUNTRY_CODE + " TEXT NOT NULL, " +
                        StationEntry.STATION_NAME + " TEXT NOT NULL, " +
                        StationEntry.STATION_UIC_CODE + " INTEGER UNIQUE NOT NULL, " +
                        StationEntry.LATITUDE + " REAL, " +
                        StationEntry.LONGITUDE + " REAL, " +
                        StationEntry.IS_TRAIN_STATION + " INTEGER NOT NULL DEFAULT 1 " +
                        " ); ";
        // create the table
        db.execSQL(SQL_CREATE_STATION_TABLE);
    }


    /**
     * Function used to migrate database to new version. Only called when
     * DATABASE_VERSION is bumped.
     *
     * @param db The database to upgrade
     * @param oldVersion Old database version
     * @param newVersion New database version (should be DATABASE_VERSION)
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // as we only cache data coming from Rata api, on upgrade
        // we can just discard the old table and then recreate tables

        db.execSQL("DROP TABLE IF EXISTS " + StationEntry.TABLE_NAME);

        onCreate(db);
    }
}
