package tk.luryus.ratajuttu.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.Set;

import tk.luryus.ratajuttu.provider.RataContract.StationEntry;

public class RataProvider extends ContentProvider {

    /**
     * An UriMatcher used to match the content uris and choose the right content to provide
     */
    private final static UriMatcher sUriMatcher = buildUriMatcher();
    /**
     * A database helper instance used to access the database
     */
    private RataDbHelper mDbHelper;

    /**
     * All the possible Uri "types" that sUriMatcher will match to
     */
    private static final int MATCH_STATIONS = 200;
    private static final int MATCH_STATIONS_WITH_FAVORITES = 201;
    private static final int MATCH_STATION_WITH_CODE = 202;

    /**
     * A selection string for selecting station with code Station.station_short_code = ?
     */
    private final static String sStationWithCodeSelection =
            StationEntry.TABLE_NAME + "." + StationEntry.STATION_SHORT_CODE + " = ? ";


    /**
     * Builds a new UriMatcher
     *
     * @return new UriMatcher instance
     */
    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = RataContract.CONTENT_AUTHORITY;

        // add Uris for all uri types we use
        matcher.addURI(authority, RataContract.PATH_STATION, MATCH_STATIONS);
        matcher.addURI(authority, RataContract.PATH_STATION + "/favorites",
                MATCH_STATIONS_WITH_FAVORITES); // todo check this out
        matcher.addURI(authority, RataContract.PATH_STATION + "/*", MATCH_STATION_WITH_CODE);

        return matcher;
    }

    /**
     * A helper function to query a single station with specified short code (well, the content uri actually).
     *
     * @param uri The content uri containing the shortcode
     * @param projection The columns to return
     * @param sortOrder The order of the return values (the single station. so does not do anything)
     * @return A cursor containing the single station
     */
    private Cursor getStationWithShortCode(Uri uri, String[] projection, String sortOrder) {
        // parse the code out of the uri
        String shortCode = StationEntry.getStationShortCodeFromUri(uri);

        return mDbHelper.getReadableDatabase().query(
                StationEntry.TABLE_NAME,
                projection,
                sStationWithCodeSelection,
                new String[]{shortCode},
                null,
                null,
                sortOrder
        );
    }

    /**
     * A helper function which does a bunch of ugly stuff to query the stations as a union, so that the favorite
     * stations can be first in the list and the full alphabetical list comes after.
     *
     * @param projection The columns to return. This can also contain {@link StationEntry#IS_FAVORITE}
     * @param sortOrder The order to return the rows in. This is applied both to the favorite stations and the ones
     *                  after it
     * @param selection Optional filtering (for example for search)
     * @param selArgs Params for selection
     * @return A cursor
     */
    private Cursor getStationsWithFavorites(String[] projection, String sortOrder, String selection, String[] selArgs) {
        // projection contains the columns, make a comma-separated list out of them
        final String projectionList = TextUtils.join(", ", projection);

        // get all the short codes of the favorite stations from the prefs
        final Set<String> favoriteSet = getContext().getSharedPreferences("favorites", Context.MODE_PRIVATE)
                                              .getStringSet("station_favorites", null);
        // generate the selection clause
        String favoriteWhere = null;
        if (favoriteSet != null && !favoriteSet.isEmpty()) {
            final String[] favCodes = favoriteSet.toArray(new String[favoriteSet.size()]);
            for (int i = 0; i < favCodes.length; i++) {
                // wrap the short codes in quotes
                favCodes[i] = "\"" + favCodes[i] + "\"";
            }

            favoriteWhere =
                    " WHERE " + StationEntry.STATION_SHORT_CODE + " IN ( " + TextUtils.join(", ", favCodes) + " ) ";
        }

        String query = "SELECT " + projectionList + " FROM ( ";
        if (favoriteWhere != null) {
            // if the favorite clause exists, return the favorite trains before the alphabetical list
            query += "SELECT 1 AS " + StationEntry.IS_FAVORITE + ", *" +
                    " FROM " + StationEntry.TABLE_NAME + favoriteWhere +
                    " UNION ALL ";
        }

        // generate a selection clause for the possible parameter selection
        String selWhere = selection != null ? " WHERE " + selection : "";
        query += "SELECT 0 AS " + StationEntry.IS_FAVORITE + ", *" +
                " FROM " + StationEntry.TABLE_NAME +
                " ORDER BY " + StationEntry.IS_FAVORITE + " DESC, " + sortOrder + " ) a" +
                selWhere;

        // fire the query
        return mDbHelper.getReadableDatabase().rawQuery(query, selArgs);
    }

    @Override
    public boolean onCreate() {
        // create db helper
        mDbHelper = new RataDbHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {

        // return the content type for given uri
        switch (sUriMatcher.match(uri)) {
            case MATCH_STATIONS:
            case MATCH_STATIONS_WITH_FAVORITES:
                // multiple station entries
                return StationEntry.CONTENT_DIR_TYPE;
            case MATCH_STATION_WITH_CODE:
                // one station entry
                return StationEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown content uri: " + uri.toString());
        }

    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {
        Cursor returnCursor;

        // find out uri type and do things accordingly
        switch (sUriMatcher.match(uri)) {
            case MATCH_STATIONS:
                returnCursor = mDbHelper.getReadableDatabase().query(
                        StationEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case MATCH_STATIONS_WITH_FAVORITES:
                returnCursor = getStationsWithFavorites(projection, sortOrder, selection, selectionArgs);
                break;

            case MATCH_STATION_WITH_CODE:
                returnCursor = getStationWithShortCode(uri, projection, sortOrder);
                break;

            default:
                throw new UnsupportedOperationException("Unknown content uri: " + uri.toString());
        }

        returnCursor.setNotificationUri(getContext().getContentResolver(), uri);

        return returnCursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;

        // this makes delete all rows return the number of rows deleted
        if (null == selection) selection = "1";

        switch (match) {
            case MATCH_STATIONS:
                rowsDeleted = db.delete(StationEntry.TABLE_NAME, selection, selectionArgs);
                break;

            default:
                throw new UnsupportedOperationException("Unknown content uri: " + uri.toString());
        }

        // Because a null deletes all rows
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsDeleted;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case MATCH_STATIONS: {
                long _id = db.insertWithOnConflict(StationEntry.TABLE_NAME, null,
                        values, SQLiteDatabase.CONFLICT_REPLACE);
                if (_id > 0) {
                    returnUri = StationEntry.buildStationUriWithId(_id);
                } else {
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                }
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown content uri: " + uri.toString());
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int bulkInsert(Uri uri, @NonNull ContentValues[] values) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case MATCH_STATIONS:
                db.beginTransaction();
                int returnCount = 0;
                try {
                    for (ContentValues value : values) {
                        long _id = db.insertWithOnConflict(StationEntry.TABLE_NAME, null,
                                value, SQLiteDatabase.CONFLICT_REPLACE);
                        if (_id != -1) {
                            returnCount++;
                        }
                    }
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                getContext().getContentResolver().notifyChange(uri, null);
                return returnCount;
            default:
                return super.bulkInsert(uri, values);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case MATCH_STATIONS:
                rowsUpdated = db.update(StationEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown content uri: " + uri.toString());
        }

        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }
}
