package tk.luryus.ratajuttu.util;

import android.content.Context;

import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Utility functions for handling dates.
 *
 * Created by Lauri Koskela on 12/09/15.
 */
public class DateUtils {


    /**
     * Formats given date to a pretty format that can be used in various train lists. Basically simple hh:mm
     * time-only format, in current user's time zone and 12/24-hour format.
     *
     * @param date The date to format
     * @param context The application context. Used to get proper time format.
     *
     * @return the formatted date
     */
    public static String getTrainListFormattedDate(Date date, Context context) {
        DateFormat df = android.text.format.DateFormat.getTimeFormat(context);
        df.setTimeZone(TimeZone.getDefault());

        return df.format(date);
    }
}
