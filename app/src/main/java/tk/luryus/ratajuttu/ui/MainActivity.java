package tk.luryus.ratajuttu.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import tk.luryus.ratajuttu.R;
import tk.luryus.ratajuttu.ui.adapter.TabAdapter;
import tk.luryus.ratajuttu.ui.fragment.LiveTrainListFragment;
import tk.luryus.ratajuttu.ui.fragment.StationListFragment;

/**
 * The app's main activity which contains the station list and the train list
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "RataJuttu.MainActivity";

    /**
     * The toolbar (action bar) of this activity
     */
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // inflate the layout straight away
        setContentView(R.layout.activity_main);

        // set up the toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(mToolbar);

        // add the fragments to the viewpager and we're done here
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager_main);
        if (viewPager != null) {
            setupViewPager(viewPager);
            TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout_main);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    /**
     * A helper method which populates the ViewPager with appropriate fragments.
     *
     * @param viewPager the view pager to add views to
     */
    private void setupViewPager(ViewPager viewPager) {
        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());

        adapter.addFragment(StationListFragment.newInstance(), getString(R.string.stations_title));
        adapter.addFragment(LiveTrainListFragment.newInstance(), getString(R.string.trains_title));

        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            // fire up the settings activity
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
