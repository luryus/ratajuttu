package tk.luryus.ratajuttu.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import tk.luryus.ratajuttu.data.LiveTrainDataHolder;

/**
 * An adapter for showing the live trains in the LiveTrainsFragment RecyclerView.
 *
 * Created by Lauri Koskela on 11/09/15.
 */
public class LiveTrainListAdapter extends RecyclerView.Adapter<LiveTrainListAdapter.ViewHolder> {

    private ArrayList<LiveTrainDataHolder> mContent;

    public LiveTrainListAdapter() {
        super();

        mContent = new ArrayList<>();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.getTrainTitleTextView().setText(mContent.get(position).getTrainTitle());
    }

    @Override
    public int getItemCount() {
        return mContent.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // we always use the same cell layout
        View newView = LayoutInflater
                .from(parent.getContext())
                .inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ViewHolder(newView);
    }

    public ArrayList<LiveTrainDataHolder> getContent() {
        return mContent;
    }

    /**
     * A helper class that holds the views for a cell representing a live train in the live trains list.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        /**
         * The TextView which contains the title of the train (IC123, R, etc.)
         */
        private TextView mTrainTitleTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            mTrainTitleTextView = (TextView) itemView.findViewById(android.R.id.text1);

            // todo bind item click listener
        }

        /**
         * Get the train title text view.
         *
         * @return the train title text view.
         */
        public TextView getTrainTitleTextView() {
            return mTrainTitleTextView;
        }
    }
}
