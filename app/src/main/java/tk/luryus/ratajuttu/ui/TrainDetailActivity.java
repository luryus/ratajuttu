package tk.luryus.ratajuttu.ui;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import tk.luryus.ratajuttu.R;
import tk.luryus.ratajuttu.ui.fragment.TrainDetailFragment;

public class TrainDetailActivity extends AppCompatActivity implements ToolbarChangeListener {

    public static final String TAG = "TrainDetailActivity";

    public static final String EXTRA_ARGUMENT_BUNDLE = "arguments_bundle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_detail);

        Bundle args = getIntent().getBundleExtra(EXTRA_ARGUMENT_BUNDLE);
        if (args != null) {
            TrainDetailFragment frag = TrainDetailFragment.newInstance(args);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, frag)
                    .commit();
        } else {
            Log.e(TAG, "Invalid arguments in intent");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_train_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        } else if (id == android.R.id.home) {
            // catch the home button and use it as back
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onToolbarChange(Toolbar tb) {
        setSupportActionBar(tb);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }
}
