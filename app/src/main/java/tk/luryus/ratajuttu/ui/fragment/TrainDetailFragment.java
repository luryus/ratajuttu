package tk.luryus.ratajuttu.ui.fragment;

import android.content.Context;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import rx.Observable;
import rx.android.app.AppObservable;
import rx.schedulers.Schedulers;
import tk.luryus.ratajuttu.R;
import tk.luryus.ratajuttu.provider.RataContract.StationEntry;
import tk.luryus.ratajuttu.rata.Rata;
import tk.luryus.ratajuttu.rata.entity.Cause;
import tk.luryus.ratajuttu.rata.entity.TimeTableRow;
import tk.luryus.ratajuttu.rata.entity.Train;
import tk.luryus.ratajuttu.ui.ToolbarChangeListener;

/**
 * A placeholder fragment containing a simple view.
 */
public class TrainDetailFragment extends Fragment {

    public static final String TAG = "TrainDetailFragment";

    public static final String ARG_TRAIN_NUMBER = "train_number";
    public static final String ARG_DEPARTURE_DATE = "departure_date";

    private int mTrainNumber;
    private String mDepartureDate;
    private Train mTrain;
    private ArrayList<TrainStopData> mStopData;

    private LinearLayout mStopsContainer;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;

    private PorterDuffColorFilter mGrayFilter;
    private PorterDuffColorFilter mPrimaryFilter;

    public static TrainDetailFragment newInstance(Bundle arguments) {
        TrainDetailFragment fragment = new TrainDetailFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public TrainDetailFragment() {
        // required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mTrainNumber = args.getInt(ARG_TRAIN_NUMBER);
            mDepartureDate =args.getString(ARG_DEPARTURE_DATE);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        // todo implement menu
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
        // todo implement action handling
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_train_detail, container, false);

        mStopsContainer = (LinearLayout) rootView.findViewById(R.id.linearlayout_train_stops_container);

        Toolbar tb = (Toolbar) rootView.findViewById(R.id.toolbar_train_detail);
        mCollapsingToolbarLayout =
                (CollapsingToolbarLayout) rootView.findViewById(R.id.collapsing_toolbar_train_detail);

        mGrayFilter = new PorterDuffColorFilter(getResources().getColor(R.color.colorTrainFutureLine), PorterDuff.Mode.MULTIPLY);
        mPrimaryFilter = new PorterDuffColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        try {
            ((ToolbarChangeListener) getActivity()).onToolbarChange(tb);
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() +
                    " must implement ToolbarChangeListener");
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        fetchLiveTrainInformation();

        super.onActivityCreated(savedInstanceState);
    }

    private void fetchLiveTrainInformation() {

        mStopsContainer.removeAllViews();
        mStopData = new ArrayList<>();
        AppObservable.bindSupportFragment(this, Rata.getApiService().getLiveTrain(mTrainNumber, mDepartureDate))
                .flatMap(trains -> Observable.just(trains.get(0)))  // rata api returns array, get the only item
                .doOnNext(train -> mTrain = train)  // save the single item
                .finallyDo(() -> {  // finally remove unnecessary data from the single train
                    // mTrain can be null if retrofit gives no response
                    if (mTrain != null) {
                        // destroy the time table from the train, we save it ourselves
                        mTrain.setTimeTableRows(null);
                    }
                })
                .flatMap(train -> {
                    for (int i = 0; i < train.getTimeTableRows().length; i++) {
                        TimeTableRow ttr = train.getTimeTableRows()[i];
                        if (i == 0) {
                            ttr.setIndex(0);
                        } else {
                            TimeTableRow previous = train.getTimeTableRows()[i-1];
                            if (ttr.getStationShortCode().equals(previous.getStationShortCode())) {
                                train.getTimeTableRows()[i].setIndex(previous.getIndex());
                            } else {
                                train.getTimeTableRows()[i].setIndex(i);
                            }
                        }
                    }
                    return Observable.from(train.getTimeTableRows());
                }) // get the timetable
                .filter(TimeTableRow::isTrainStopping)  // get only stops (todo commercial?)
                .groupBy(TimeTableRow::getIndex) // make groups by station (arrival & departure)
                .concatMap(stopGroupObservable -> { // build data objects from the groups
                    final TrainStopData stopData = new TrainStopData();
                    return stopGroupObservable.reduce(stopData, (data, ttr) -> {
                        if ("ARRIVAL".equals(ttr.getType())) {
                            data.scheduledArrival = ttr.getScheduledTime();
                            data.actualArrival = ttr.getActualTime();
                            data.estimatedArrival = ttr.getLiveEstimateTime();
                            data.arrivalDifferenceInMinutes = ttr.getDifferenceInMinutes();
                        } else {
                            data.scheduledDeparture = ttr.getScheduledTime();
                            data.actualDeparture = ttr.getActualTime();
                            data.estimatedDeparture = ttr.getLiveEstimateTime();
                            data.departureDifferenceInMinutes = ttr.getDifferenceInMinutes();
                        }

                        if (data.track == null) {
                            data.track = ttr.getCommercialTrack();
                            data.setStation(ttr.getStationShortCode(), getActivity());
                        }

                        if (ttr.getCauses() != null) {
                            data.addCauses(ttr.getCauses());
                        }
                        return data;
                    });
                })
                .doOnNext(data -> {
                    calculateTrainLocationFlags(data);
                    mStopData.add(data);
                }) // add the data item to array in background thread
                .subscribeOn(Schedulers.io())
                .subscribe(
                        // onNext
                        data -> {
                            View v = buildStopCell(data);
                            mStopsContainer.addView(v);
                        },
                        // onError
                        throwable -> Log.e(TAG, "fetchLiveTrainInformation failed\n" + Log.getStackTraceString(throwable)),
                        // onComplete
                        () -> {
                            mCollapsingToolbarLayout.setTitle(mTrain.getTrainTitle());

                            View lastChild = mStopsContainer.getChildAt(mStopsContainer.getChildCount() - 1);
                            if (lastChild != null) {
                                lastChild.findViewById(R.id.linearlayout_stop_inset_content).setBackground(null);
                            }
                        }
                );
    }

    private static void calculateTrainLocationFlags(TrainStopData data) {
        if (data.scheduledArrival == null && data.scheduledDeparture != null) {
            data.trainLocation |= TrainStopData.START_POINT;

            if (data.actualDeparture != null) {
                data.trainLocation |= TrainStopData.PAST_STATION;
            } else {
                data.trainLocation |= TrainStopData.ON_STATION;
            }
        } else if (data.scheduledDeparture == null && data.scheduledArrival != null) {
            data.trainLocation |= TrainStopData.END_POINT;

            if (data.actualArrival != null) {
                data.trainLocation |= TrainStopData.ON_STATION;
            } else {
                data.trainLocation |= TrainStopData.NOT_ARRIVED;
            }
        } else {
            if (data.actualArrival == null && data.actualDeparture == null) {
                data.trainLocation |= TrainStopData.NOT_ARRIVED;
            } else if (data.actualArrival != null && data.actualDeparture == null) {
                data.trainLocation |= TrainStopData.ON_STATION;
            } else {
                data.trainLocation |= TrainStopData.PAST_STATION;
            }
        }
    }

    @NonNull
    private View buildStopCell(TrainStopData data) {
        View v = getActivity().getLayoutInflater()
                .inflate(R.layout.train_cell_stop, mStopsContainer, false);

        ((TextView) v.findViewById(R.id.textview_station_name)).setText(data.stationName);

        ((TextView) v.findViewById(R.id.textview_scheduled_time)).setText(TrainStopData.formatTime(data.getScheduledTime(), getActivity()));

        final Date realTime = data.getRealTime();
        if (realTime != null) {
            TextView approxTextView = (TextView) v.findViewById(R.id.textview_real_time);
            TrainStopData.RealTimeType rtt = data.getRealTimeType();
            boolean isLate = data.getIsLate();

            if (!isLate) {
                if (rtt != TrainStopData.RealTimeType.ACTUAL_DEPARTURE && rtt != TrainStopData.RealTimeType.ACTUAL_ARRIVAL) {
                    approxTextView.setText(getText(R.string.train_on_time));
                } else if (rtt == TrainStopData.RealTimeType.ACTUAL_DEPARTURE) {
                    approxTextView.setText(getText(R.string.train_actual_departure)
                            + " " + TrainStopData.formatTime(realTime, getActivity()));
                } else {
                    approxTextView.setText(getText(R.string.train_actual_arrival)
                            + " " + TrainStopData.formatTime(realTime, getActivity()));
                }
            } else {
                approxTextView.setTextColor(getResources().getColor(R.color.train_late));

                if (rtt == TrainStopData.RealTimeType.ACTUAL_DEPARTURE) {
                    approxTextView.setText(getText(R.string.train_actual_departure)
                            + " " + TrainStopData.formatTime(realTime, getActivity()));;
                } else if (rtt == TrainStopData.RealTimeType.ACTUAL_ARRIVAL) {
                    approxTextView.setText(getText(R.string.train_actual_arrival)
                            + " " + TrainStopData.formatTime(realTime, getActivity()));
                } else if (rtt == TrainStopData.RealTimeType.ESTIMATED_DEPARTURE) {
                    approxTextView.setText(getText(R.string.train_estimated_departure)
                            + " " + TrainStopData.formatTime(realTime, getActivity()));
                } else if (rtt == TrainStopData.RealTimeType.ESTIMATED_ARRIVAL) {
                    approxTextView.setText(getText(R.string.train_estimated_arrival)
                            + " " + TrainStopData.formatTime(realTime, getActivity()));
                }
            }

        }

        if ((data.trainLocation & TrainStopData.START_POINT) != 0)
            v.findViewById(R.id.stop_line_top).setVisibility(View.INVISIBLE);
        else if ((data.trainLocation & TrainStopData.END_POINT) != 0)
            v.findViewById(R.id.stop_line_bottom).setVisibility(View.INVISIBLE);

        if ((data.trainLocation & TrainStopData.PAST_STATION) != 0
                || (data.trainLocation & TrainStopData.ON_STATION) != 0) {
            v.findViewById(R.id.stop_line_top).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            ((ImageView) v.findViewById(R.id.stop_blob)).setColorFilter(mPrimaryFilter);
        }

        if ((data.trainLocation & TrainStopData.PAST_STATION) != 0) {
            v.findViewById(R.id.stop_line_bottom).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            ((ImageView) v.findViewById(R.id.stop_blob)).setColorFilter(mPrimaryFilter);
        }

        if ((data.trainLocation & TrainStopData.NOT_ARRIVED) != 0) {
            ((ImageView) v.findViewById(R.id.stop_blob)).setColorFilter(mGrayFilter);
        }

        return v;
    }

    private static class TrainStopData {

        public static final byte START_POINT    = 0b00000001;
        public static final byte END_POINT      = 0b00000010;
        public static final byte ON_STATION     = 0b00000100;
        public static final byte NOT_ARRIVED    = 0b00001000;
        public static final byte PAST_STATION   = 0b00010000;

        public enum RealTimeType {
            ACTUAL_DEPARTURE, ACTUAL_ARRIVAL, ESTIMATED_DEPARTURE, ESTIMATED_ARRIVAL, INVALID
        }

        public Date scheduledArrival;
        public Date actualArrival;
        public Date estimatedArrival;
        public Integer arrivalDifferenceInMinutes;

        public Date scheduledDeparture;
        public Date actualDeparture;
        public Date estimatedDeparture;
        public Integer departureDifferenceInMinutes;

        public RealTimeType realTimeType;

        public String track;
        public String stationName;
        public String stationShortCode;
        public ArrayList<Cause> causes;

        public byte trainLocation;

        public void addCauses(List<Cause> causes) {
            if (this.causes == null) {
                this.causes = new ArrayList<>(1);
            }

            this.causes.addAll(causes);
        }

        public void setStation(String stationShortCode, Context context) {
            this.stationShortCode = stationShortCode;

            String[] columns = {
                    StationEntry.STATION_NAME
            };

            Cursor c = context.getContentResolver().query(
                    StationEntry.buildStationUriWithShortCode(stationShortCode),
                    columns,
                    null,
                    null,
                    null
            );

            if (c != null) {
                if (c.moveToFirst()) {
                    this.stationName = c.getString(0);
                }
                c.close();
            }
        }

        public static String formatTime(Date date, Context ctx) {
            DateFormat df = android.text.format.DateFormat.getTimeFormat(ctx);
            df.setTimeZone(TimeZone.getDefault());

            return df.format(date);
        }

        public Date getRealTime() {
            if (scheduledDeparture != null) {
                if (actualDeparture != null) {
                    realTimeType = RealTimeType.ACTUAL_DEPARTURE;
                    return actualDeparture;
                } else if (estimatedDeparture != null) {
                    realTimeType = RealTimeType.ESTIMATED_DEPARTURE;
                    return estimatedDeparture;
                }
            } else if (scheduledArrival != null) {
                if (actualArrival != null) {
                    realTimeType = RealTimeType.ACTUAL_ARRIVAL;
                    return actualArrival;
                } else if (estimatedArrival != null) {
                    realTimeType = RealTimeType.ESTIMATED_ARRIVAL;
                    return estimatedArrival;
                }
            } else {
                realTimeType = RealTimeType.INVALID;
            }

            realTimeType = RealTimeType.INVALID;
            return null;
        }

        public RealTimeType getRealTimeType() {
            if (this.realTimeType == null)
                getRealTime();

            return realTimeType;
        }

        public Date getScheduledTime() {
            return firstNotNull(scheduledDeparture, scheduledArrival);
        }

        public boolean getIsLate() {
            if (departureDifferenceInMinutes != null) {
                return departureDifferenceInMinutes > 0;
            } else if (arrivalDifferenceInMinutes > 0) {
                return arrivalDifferenceInMinutes > 0;
            }

            return false;
        }



        @SafeVarargs
        private static <T> T firstNotNull(T... objects) {
            for (T o : objects) {
                if (o != null) return o;
            }

            return null;
        }
    }
}
