package tk.luryus.ratajuttu.ui.adapter;


import android.content.Context;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.text.Collator;
import java.util.Locale;

import tk.luryus.ratajuttu.R;
import tk.luryus.ratajuttu.ui.fragment.StationListFragment;
import tk.luryus.ratajuttu.ui.widget.FavoriteAlphabetIndexer;

/**
 * An adapter that handles showing all the train stations in a recycler view.
 */
public class StationListAdapter extends CursorRecyclerViewAdapter<StationListAdapter.ViewHolder>
        implements SectionIndexer {

    /**
     * A listener that handles the item clicks in this list.
     */
    private final ItemClickListener mItemClickListener;
    /**
     * An indexer that makes the favorite + alphabetical sections happen.
     */
    private final FavoriteAlphabetIndexer mFavAlphabetIndexer;
    /**
     * The color filter used to make the star besides the first favorite item red.
     */
    private final PorterDuffColorFilter mColorFilter;

    /**
     * StationListAdapter constructor.
     *
     * @param context  Context for this adapter
     * @param cursor   The cursor containing the data this adapter should handle
     * @param listener A listener that handles the item clicks of the list
     */
    public StationListAdapter(Context context, Cursor cursor, ItemClickListener listener) {
        super(context, cursor);
        mItemClickListener = listener;

        // the names are Finnish station names, force Finnish locale for sections
        // so that ä, ö, etc. show up correctly
        final Collator collator = Collator.getInstance(new Locale("fi_FI"));
        collator.setStrength(Collator.PRIMARY);

        // make new indexer. The # is used for favorites.
        mFavAlphabetIndexer = new FavoriteAlphabetIndexer(cursor, StationListFragment.COL_STATION_NAME,
                StationListFragment.COL_IS_FAVORITE, "# ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ", collator);
        mFavAlphabetIndexer.setCursor(cursor);

        // we need a color filter for the star icon besides the favorites section in the list, make and save it now
        mColorFilter = new PorterDuffColorFilter(context.getResources().getColor(R.color.colorAccent),
                PorterDuff.Mode.MULTIPLY);
    }

    //region ViewHolder things
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        // set the name in the list, and set hidden station short code for click handling
        viewHolder.getNameTextView().setText(cursor.getString(StationListFragment.COL_STATION_NAME));
        viewHolder.setStationShortCode(cursor.getString(StationListFragment.COL_STATION_SHORT_CODE));

        final int pos = cursor.getPosition();
        final int section = getSectionForPosition(pos);
        if (pos == getPositionForSection(section)) {
            // pos is first in section

            // if section is #, show star, else show section
            String sectionStr = getSections()[section].toString();
            if ("#".equals(sectionStr)) {
                viewHolder.getSectionStarImageView().setColorFilter(mColorFilter);
                viewHolder.getSectionStarImageView().setVisibility(View.VISIBLE);
                viewHolder.getSectionTextView().setVisibility(View.GONE);
            } else {
                viewHolder.getSectionTextView().setText(sectionStr);
                viewHolder.getSectionTextView().setVisibility(View.VISIBLE);
                viewHolder.getSectionStarImageView().setVisibility(View.GONE);
            }
        } else {
            // not first in section, do not show star or letter
            viewHolder.getSectionTextView().setVisibility(View.GONE);
            viewHolder.getSectionStarImageView().setVisibility(View.GONE);
        }

        if (pos + 1 == getPositionForSection(section + 1) && pos != cursor.getCount() - 1) {
            // pos is last in section, but not last in the whole list
            viewHolder.itemView.setTag(R.id.station_list_item_has_divider, true);
        } else {
            // not last in section, so no divider
            viewHolder.itemView.setTag(R.id.station_list_item_has_divider, false);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // at the time always creating a normal view
        View newView = LayoutInflater.from(parent.getContext())
                                     .inflate(R.layout.list_item_station, parent, false);

        return new ViewHolder(newView);
    }

    /**
     * A helper class that holds the views (and some data) for a cell in the station list. This makes the recycler view
     * faster as new cells can be reused when scrolling.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        /**
         * The actual TextView that contains the station's name.
         */
        private TextView mNameTextView;
        /**
         * The one-letter-wide accentColor text view besides the first item in the section.
         */
        private TextView mSectionTextView;
        /**
         * The accentColor star icon besides the first favorite item.
         */
        private ImageView mSectionStarImageView;
        /**
         * The station short code (e.g. TL) of the station this cell represents. Not a view, not shown anywhere, but
         * saved for click handling.
         */
        private String mStationShortCode;

        /**
         * ViewHolder constructor.
         *
         * @param itemView The cell view to hold subviews for
         */
        public ViewHolder(View itemView) {
            super(itemView);

            mNameTextView = (TextView) itemView.findViewById(R.id.textview_station_name);
            mSectionTextView = (TextView) itemView.findViewById(R.id.textview_station_section_letter);
            mSectionStarImageView = (ImageView) itemView.findViewById(R.id.imageview_station_section_star);

            // we are using the adapter's ItemClickListener interface to handle the clicks, passing the short code as param
            itemView.setOnClickListener(v -> mItemClickListener.onItemClick(mStationShortCode));
        }

        /**
         * Get the station name text view.
         *
         * @return the station name text view.
         */
        public TextView getNameTextView() {
            return mNameTextView;
        }

        /**
         * Get the one-letter section text view.
         *
         * @return the one-letter section text view.
         */
        public TextView getSectionTextView() {
            return mSectionTextView;
        }

        /**
         * Get the favorite star section image view.
         *
         * @return the favorite star section image view.
         */
        public ImageView getSectionStarImageView() {
            return mSectionStarImageView;
        }

        /**
         * Set the station short code of the station this view represents.
         *
         * @param stationShortCode The short code of the station
         */
        public void setStationShortCode(String stationShortCode) {
            mStationShortCode = stationShortCode;
        }
    }

    /**
     * A helper interface for handling the list item clicks in the station list.
     */
    public interface ItemClickListener {

        /**
         * Fired when a item was clicked.
         *
         * @param stationShortCode The short code of the station that was clicked
         */
        void onItemClick(String stationShortCode);
    }
    //endregion

    //region Section indexer
    @Override
    public Object[] getSections() {
        return mFavAlphabetIndexer.getSections();
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mFavAlphabetIndexer.getPositionForSection(sectionIndex);
    }

    @Override
    public int getSectionForPosition(int position) {
        if (position >= getItemCount()) {
            position = getItemCount() - 1;
        }

        return mFavAlphabetIndexer.getSectionForPosition(position);
    }
    //endregion

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        // before swapping the cursor, inform the indexer about the change
        mFavAlphabetIndexer.setCursor(newCursor);
        return super.swapCursor(newCursor);
    }

    @Override
    public long getItemId(int position) {
        /*
        So. The query returns some station rows multiple times, because favorites are returned both in
        the favorite section and the alphabetical list. That means we will get overlapping _id values,
        which will not work with the default implementation of CursorRecyclerViewAdapter.
        Because we will not actually use the _ids for anything, we can just return the position as the
        item's unique id in the list. Seems to work just fine.
        */
        return position;
    }
}
