package tk.luryus.ratajuttu.ui;

import android.support.v7.widget.Toolbar;

/**
 * Created by luryus on 25/06/15.
 */
public interface ToolbarChangeListener {

    void onToolbarChange(Toolbar tb);
}
