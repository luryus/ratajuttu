package tk.luryus.ratajuttu.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import tk.luryus.ratajuttu.R;
import tk.luryus.ratajuttu.data.LiveTrainRefreshManager;
import tk.luryus.ratajuttu.ui.adapter.LiveTrainListAdapter;

/**
 * A simple {@link Fragment} subclass. Use the {@link
 * LiveTrainListFragment#newInstance} factory method to create an instance of this fragment.
 */
public class LiveTrainListFragment extends Fragment implements LiveTrainRefreshManager.LiveTrainRefreshCallbacks {

    private RecyclerView mRecyclerView;
    private LiveTrainListAdapter mAdapter;

    private ProgressBar mProgressBar;

    private LiveTrainRefreshManager mManager;

    /**
     * Use this factory method to create a new instance of this fragment using the provided parameters.
     **
     * @return A new instance of fragment LiveTrainListFragment.
     */
    public static LiveTrainListFragment newInstance() {
        return new LiveTrainListFragment();
    }

    public LiveTrainListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragView = inflater.inflate(R.layout.fragment_live_train_list, container, false);
        mProgressBar = (ProgressBar) fragView.findViewById(R.id.progressbar_live_train_list);
        mRecyclerView = (RecyclerView) fragView.findViewById(R.id.recyclerview_live_trains);
        setupRecyclerView();
        mManager = new LiveTrainRefreshManager(this, mAdapter);
        return fragView;
    }

    private void setupRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mRecyclerView.getContext()));
        mAdapter = new LiveTrainListAdapter();
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mManager.refreshLiveTrains();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.live_train_list, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.action_refresh:
                mManager.refreshLiveTrains();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void refreshStarted() {
        mProgressBar.setEnabled(true);
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void refreshCompleted() {
        mRecyclerView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mProgressBar.setEnabled(false);
    }
}
