package tk.luryus.ratajuttu.ui.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import tk.luryus.ratajuttu.R;
import tk.luryus.ratajuttu.data.FetchStationsTask;
import tk.luryus.ratajuttu.provider.RataContract.StationEntry;
import tk.luryus.ratajuttu.ui.StationDetailActivity;
import tk.luryus.ratajuttu.ui.adapter.StationListAdapter;
import tk.luryus.ratajuttu.ui.widget.InsetDividerItemDecoration;

/**
 * A fragment representing a list of Items.
 */
public class StationListFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor>, StationListAdapter.ItemClickListener,
                   SearchView.OnQueryTextListener {

    /**
     * The adapter that handles the station recycler view content
     */
    private StationListAdapter mStationAdapter;
    /**
     * Id for the station loader
     */
    private static final int STATION_LOADER = 0;
    /**
     * The loader's filter (search) argument bundle key
     */
    private static final String LOADER_ARG_FILTER_STRING = "station_filter_string";
    /**
     * The columns which we will get from the db for the stations list
     * <p>
     * If this is changed, the column ids below must also be changed
     */
    public static final String[] STATION_LIST_COLUMNS = new String[]{
            StationEntry._ID,
            StationEntry.STATION_SHORT_CODE,
            StationEntry.STATION_NAME,
            StationEntry.IS_FAVORITE
    };

    /**
     * The column indexes for the projection ({@link #STATION_LIST_COLUMNS})
     */
    public static final int COL_ID = 0;
    public static final int COL_STATION_SHORT_CODE = 1;
    public static final int COL_STATION_NAME = 2;
    public static final int COL_IS_FAVORITE = 3;


    private boolean mShowOnlyTrainStations;
    /**
     * A factory method for this fragment.
     *
     * Pretty simple, takes no parameters (yay)
     *
     * @return a new instance of StationListFragment
     */
    public static StationListFragment newInstance() {
        return new StationListFragment();
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the fragment (e.g. upon screen orientation
     * changes).
     *
     * Do not use this, instead use ({@link #newInstance()}
     */
    public StationListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mStationAdapter = new StationListAdapter(getActivity(), null, this);

        View rootView = inflater.inflate(R.layout.fragment_station_list, container, false);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_stations);
        recyclerView.setLayoutManager(new LinearLayoutManager(container.getContext()));
        recyclerView.setAdapter(mStationAdapter);

        recyclerView.addItemDecoration(new StationItemDividerDecoration(
                ContextCompat.getDrawable(getActivity(), R.drawable.list_divider)));

        FetchStationsTask task = new FetchStationsTask(getActivity());
        task.execute(false);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        getLoaderManager().initLoader(STATION_LOADER, null, this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean onlyTrainStations = prefs.getBoolean(getString(R.string.pref_show_only_train_stations), false);

        if (getLoaderManager().getLoader(STATION_LOADER).isStarted()) {
            if (onlyTrainStations != mShowOnlyTrainStations) {
                // store the "only train stations" preference
                mShowOnlyTrainStations = onlyTrainStations;
                getLoaderManager().restartLoader(STATION_LOADER, null, this);
            } else {
                getLoaderManager().getLoader(STATION_LOADER).onContentChanged();
            }
        }
        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.station_list, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Bundle args = new Bundle(1);
        args.putString(LOADER_ARG_FILTER_STRING, query);
        getLoaderManager().restartLoader(STATION_LOADER, args, this);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Bundle args = new Bundle(1);
        args.putString(LOADER_ARG_FILTER_STRING, newText);
        getLoaderManager().restartLoader(STATION_LOADER, args, this);
        return false;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // determine sort order (ascending alphabetical)
        String sortOrder = StationEntry.STATION_NAME + " ASC";

        String filter = args != null ? args.getString(LOADER_ARG_FILTER_STRING) : null;
        // get the "show only train stations" pref
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean onlyTrainStations = prefs.getBoolean(getString(R.string.pref_show_only_train_stations), false);

        String selection;
        String[] selArgs = null;

        if (onlyTrainStations)
            selection = StationEntry.IS_TRAIN_STATION + " = 1 ";
        else
            selection = null;

        if (filter != null && !filter.equals("")) {
            if (selection != null)
                selection += " AND ";
            else
                selection = "";

            selection += StationEntry.STATION_NAME + " LIKE ?";
            selArgs = new String[]{"%" + filter + "%"};
        }

        Uri stationContentUri = StationEntry.getStationsWithFavoritesUri();
        return new CursorLoader(
                getActivity(),
                stationContentUri,
                STATION_LIST_COLUMNS,
                selection,
                selArgs,
                sortOrder
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mStationAdapter.swapCursor(data);
        // todo maybe position changing?
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mStationAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(String stationShortCode) {
        Uri contentUri = StationEntry.buildStationUriWithShortCode(stationShortCode);

        Intent i = new Intent(getActivity(), StationDetailActivity.class).setData(contentUri);
        getActivity().startActivity(i);
    }

    private class StationItemDividerDecoration extends InsetDividerItemDecoration {

        public StationItemDividerDecoration(Drawable divider) {
            super(divider, (int) (getActivity().getResources().getDimension(R.dimen.station_list_divider_inset) + 0.5f),
                    false);
        }

        @Override
        protected boolean shouldDrawDivider(RecyclerView recyclerView, int index) {
            View v = recyclerView.getChildAt(index);
            return (Boolean) v.getTag(R.id.station_list_item_has_divider);
        }
    }

}
