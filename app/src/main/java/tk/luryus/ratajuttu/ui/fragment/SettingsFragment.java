package tk.luryus.ratajuttu.ui.fragment;

import android.os.Bundle;

import tk.luryus.ratajuttu.R;

/**
 * Created by Lauri Koskela on 29/08/15.
 */
public class SettingsFragment extends android.support.v14.preference.PreferenceFragment {

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.settings);
    }
}
