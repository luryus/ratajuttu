package tk.luryus.ratajuttu.ui.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import rx.Observable;
import rx.android.app.AppObservable;
import rx.schedulers.Schedulers;
import tk.luryus.ratajuttu.R;
import tk.luryus.ratajuttu.rata.Rata;
import tk.luryus.ratajuttu.rata.entity.Train;
import tk.luryus.ratajuttu.ui.TrainDetailActivity;
import tk.luryus.ratajuttu.ui.adapter.StationTrainListAdapter;
import tk.luryus.ratajuttu.ui.widget.InsetDividerItemDecoration;
import tk.luryus.ratajuttu.ui.widget.StationTrainItemShadowDecoration;

/**
 * A {@link Fragment} which is used to show the details of one station.
 * <p>
 * Use the {@link StationTrainListFragment#newInstance} factory method to create an instance of this fragment.
 */
public class StationTrainListFragment extends Fragment implements StationTrainListAdapter.OnTrainListClickListener {

    /**
     * The arguments key for station short code argument
     */
    private static final String ARG_STATION_CODE = "station_short_code";
    private static final String ARG_ROW_TYPE = "timetable_row_type";
    /**
     * Log tag for this fragment
     */
    private static final String TAG = "StationTrainList";
    private int mDeparting;
    private int mArrived;
    private int mArriving;
    private int mDeparted;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;

    @IntDef({
            REFRESH_TYPE_NORMAL, REFRESH_TYPE_PREVIOUS, REFRESH_TYPE_NEXT
    })
    private @interface RefreshType {

    }

    private static final int REFRESH_TYPE_NORMAL = 0;
    private static final int REFRESH_TYPE_PREVIOUS = 1;
    private static final int REFRESH_TYPE_NEXT = 2;

    /**
     * A type definition for the row types.
     */
    @StringDef({
            TIMETABLE_ROW_TYPE_ARRIVAL,
            TIMETABLE_ROW_TYPE_DEPARTURE
    })
    public @interface TimeTableRowType {

    }

    private @StationTrainListFragment.TimeTableRowType String mRowType;

    /**
     * A constant for distinguishing the timetable rows which represent train arrival to station.
     */
    public static final String TIMETABLE_ROW_TYPE_ARRIVAL = "ARRIVAL";
    /**
     * A constant for distinguishing the timetable rows which represent train departure from station.
     */
    public static final String TIMETABLE_ROW_TYPE_DEPARTURE = "DEPARTURE";

    /**
     * The short code of the station this fragment is showing details for
     */
    private String mStationCode;

    private StationTrainListAdapter mAdapter;

    /**
     * Indicates whether a refresh operation is running or not. It should be safe not to use atomic variables, because
     * this should only ever be changed in the UI thread.
     */
    private boolean mRefreshRunning = false;

    /**
     * This factory method creates a new fragment instance with bundled params
     *
     * @param stationShortCode The short code of the station
     *
     * @return A new instance of fragment StationTrainListFragment.
     */
    public static StationTrainListFragment newInstance(String stationShortCode, @TimeTableRowType String rowType) {
        StationTrainListFragment fragment = new StationTrainListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_STATION_CODE, stationShortCode);
        args.putString(ARG_ROW_TYPE, rowType);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Empty default constructor. Do NOT use this, use {@link #newInstance(String, String)}
     */
    public StationTrainListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mStationCode = args.getString(ARG_STATION_CODE);
            //noinspection ResourceType
            mRowType = args.getString(ARG_ROW_TYPE);

            if (mRowType.equals(TIMETABLE_ROW_TYPE_ARRIVAL)) {
                mArrived = 1;
                mArriving = 5;
                mDeparted = mDeparting = 0;
            } else {
                mDeparted = 1;
                mDeparting = 5;
                mArrived = mArriving = 0;
            }
        }

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.station_train_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_refresh:
                refresh(REFRESH_TYPE_NORMAL);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the base recyclerview for this fragment
        View rootView = inflater.inflate(R.layout.fragment_station_train_list, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview_station_trains);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressbar_station_train_list);
        setupRecyclerView(mRecyclerView);
        return rootView;
    }

    private void setupRecyclerView(RecyclerView rv) {
        rv.setLayoutManager(new LinearLayoutManager(rv.getContext()));
        mAdapter = new StationTrainListAdapter(this);
        rv.setAdapter(mAdapter);

        rv.addItemDecoration(new StationTrainListDividerDecoration(
                ContextCompat.getDrawable(getActivity(), R.drawable.list_divider)));

        if (!supportsViewElevation()) {
            rv.addItemDecoration(new StationTrainItemShadowDecoration((NinePatchDrawable) ContextCompat.getDrawable(
                    getActivity(), R.drawable.material_shadow_z2)));
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // start fetching train information
        refresh(REFRESH_TYPE_NORMAL);

        super.onActivityCreated(savedInstanceState);
    }

    /**
     * Fetches new train information for arriving and departing trains, and does things with the info.
     * <p>
     * This first clears the existing train cells from the cards and shows the progress indicator right away. Then new
     * train info is fetched in the background (using retrofit + rx) and the data is also processed in the background as
     * much as possible. For each fetched train, a cell view is generated in the UI thread and the cell is added to the
     * card's linearlayout.
     * <p>
     * When all trains are handled, the progress indicators are hidden and the cards are checked. If the linear layout
     * has no children, a placeholder "no trains found" cell is inserted.
     * <p>
     * If an error occurs, a toast is shown indicating the error to the user.
     */
    private void refresh(@RefreshType int type) {
        // if we are refreshing, do not allow starting another refresh
        if (mRefreshRunning) {
            return;
        }
        // we are refreshing now
        mRefreshRunning = true;

        if (type == REFRESH_TYPE_NORMAL) {
            mProgressBar.setEnabled(true);
            mProgressBar.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }

        final HashSet<Pair<Integer, String>> oldTrains = new HashSet<>(mAdapter.getContent().size());
        if (type != REFRESH_TYPE_NORMAL) {
            for (TrainDataHolder h : mAdapter.getContent()) {
                oldTrains.add(new Pair<>(h.number, h.departureDate));
            }
        }

        final boolean[] allPreviousTrainsBefore = {true};

        final ArrayList<TrainDataHolder> newContent; // init this with appropriate capacity
        if (type == REFRESH_TYPE_NORMAL) {
            newContent = new ArrayList<>(mArrived + mArriving + mDeparted + mDeparting);
        } else {
            newContent = new ArrayList<>(5);
        }

        AppObservable.bindSupportFragment(this,
                Rata.getApiService()
                    .getLiveTrainsForStation(
                            mStationCode,
                            mArrived,
                            mArriving,
                            mDeparted,
                            mDeparting))
                // filter out the old trains and sort
                .doOnNext(trains -> {
                    filterNewTrains(oldTrains, trains);

                    sortTrains(trains, mStationCode, mRowType);

                    if (type == REFRESH_TYPE_PREVIOUS) {
                        // calculate this thing, will need it later
                        allPreviousTrainsBefore[0] = areTrainsBefore(trains,
                                new Date(mAdapter.getContent().get(0).compareTime),
                                mStationCode, mRowType);
                        if (allPreviousTrainsBefore[0]) {
                            Log.d(TAG, "all previous trains were before first one");
                        } else {
                            Log.d(TAG, "all previous trains were not before first one");
                        }
                    }
                })
                // transform the array from retrofit into an observable right away
                .flatMap(Observable::from)
                // build a TrainDataHolder from each Train and swap the holder in place of the Train object
                .map(train -> buildTrainDataHolder(train, mStationCode, mRowType))
                // do all this processing in background thread
                .subscribeOn(Schedulers.io())
                .subscribe(
                        // onNext
                        newContent::add,
                        // onError
                        throwable -> {
                            Toast.makeText(getActivity(), "Error while fetching trains",
                                    Toast.LENGTH_SHORT).show();
                            Log.e(TAG, Log.getStackTraceString(throwable));
                        },
                        // onComplete
                        () -> {

                            if (type == REFRESH_TYPE_NORMAL) {
                                if (newContent.size() > 0) {
                                    mAdapter.setHasHeader(true);
                                    mAdapter.setHasFooter(true);
                                    mAdapter.setHasNoTrains(false);
                                    mAdapter.setContent(newContent);
                                } else {
                                    mAdapter.setHasHeader(false);
                                    mAdapter.setHasFooter(false);
                                    mAdapter.setHasNoTrains(true);
                                    mAdapter.setContent(newContent);
                                }
                                mAdapter.notifyDataSetChanged();

                                mRecyclerView.setVisibility(View.VISIBLE);
                                mProgressBar.setVisibility(View.GONE);
                                mProgressBar.setEnabled(false);
                            } else if (type == REFRESH_TYPE_NEXT) {
                                if (newContent.size() > 5) {
                                    // this means the timetables have changed, so do full refresh
                                    mRefreshRunning = false;
                                    mAdapter.setIsFooterLoading(false);
                                    mAdapter.notifyItemChanged(mAdapter.getItemCount() - 1);
                                    refresh(REFRESH_TYPE_NORMAL);
                                    return;
                                } else if (newContent.size() == 0) {
                                    // hide the "load next button"
                                    mAdapter.setHasFooter(false);
                                    mAdapter.notifyItemRemoved(mAdapter.getItemCount());
                                } else {
                                    // append the trains
                                    int insertRangeStart = (mAdapter.getHasHeader() ? 1 : 0) +
                                            mAdapter.getContent().size();
                                    mAdapter.getContent().addAll(newContent);
                                    mAdapter.notifyItemRangeInserted(insertRangeStart, newContent.size());
                                }

                                mAdapter.setIsFooterLoading(false);
                                mAdapter.notifyItemChanged(mAdapter.getItemCount() - 1);
                            } else if (type == REFRESH_TYPE_PREVIOUS) {
                                if (!allPreviousTrainsBefore[0]) {
                                    // this means the timetables have changed, so do full refresh
                                    Log.d(TAG, "forcing full refresh");
                                    mRefreshRunning = false;
                                    mAdapter.setIsHeaderLoading(false);
                                    mAdapter.notifyItemChanged(0);
                                    refresh(REFRESH_TYPE_NORMAL);
                                    return;
                                } else if (newContent.size() == 0) {
                                    // hide the "load previous" button
                                    mAdapter.setHasHeader(false);
                                    mAdapter.notifyItemRemoved(0);
                                } else {
                                    // add the trains before existing trains
                                    int insertRangeStart = (mAdapter.getHasHeader() ? 1 : 0);
                                    mAdapter.getContent().addAll(0, newContent);
                                    mAdapter.notifyItemRangeInserted(insertRangeStart, newContent.size());
                                }

                                mAdapter.setIsHeaderLoading(false);
                                mAdapter.notifyItemChanged(0);
                            }

                            mRefreshRunning = false;
                        }
                );
    }

    /**
     * Takes a list of trains and sorts it by the scheduled times on specified station, using stationCode and rowType
     * for calculating the order.
     * <p>
     * This can be used only for sorting the trains according to the scheduled arrival/departure times on some
     * particular station.
     *
     * @param trains      The list of trains to sort
     * @param stationCode The station code of the station to sort with
     * @param rowType     {@link #TIMETABLE_ROW_TYPE_ARRIVAL} or {@link #TIMETABLE_ROW_TYPE_DEPARTURE}
     */
    private void sortTrains(List<Train> trains, final String stationCode, final @TimeTableRowType String rowType) {
        // define the thing we want from each train for sorting them
        final EnumSet<Train.StationDataType> dataFields =
                EnumSet.of(Train.StationDataType.MOST_ACCURATE_TIME, Train.StationDataType.MOST_ACCURATE_TIME_TYPE);
        // TODO android developers think that enums are bad for performance. Should probably test the int +
        // annotations combo some time to make this possibly faster

        // sort the trains according to the scheduled time
        Collections.sort(trains, (trainA, trainB) -> {
            SimpleArrayMap<Train.StationDataType, Object> dataA =
                    trainA.getStationData(stationCode, rowType, dataFields, getActivity());
            SimpleArrayMap<Train.StationDataType, Object> dataB =
                    trainB.getStationData(stationCode, rowType, dataFields, getActivity());

            Train.TimeType typeA = (Train.TimeType) dataA.get(Train.StationDataType.MOST_ACCURATE_TIME_TYPE);
            Train.TimeType typeB = (Train.TimeType) dataB.get(Train.StationDataType.MOST_ACCURATE_TIME_TYPE);

            if (typeA != typeB) {
                if (typeA == Train.TimeType.ACTUAL) {
                    return -1;
                } else if (typeB == Train.TimeType.ACTUAL) {
                    return 1;
                }
            }

            Date dateA = (Date) dataA.get(Train.StationDataType.MOST_ACCURATE_TIME);
            Date dateB = (Date) dataB.get(Train.StationDataType.MOST_ACCURATE_TIME);

            return dateA.compareTo(dateB);
        });
    }

    /**
     * Takes in all previously loaded trains (as numbers and departure date strings) and filters them out of the list
     * of trains passed.
     * @param oldTrains The set of old train numbers and departure dates
     * @param trains The "new" train list to filter
     */
    private void filterNewTrains(HashSet<Pair<Integer, String>> oldTrains, List<Train> trains) {
        Iterator<Train> i = trains.iterator();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean filterPassengerTrains = prefs.getBoolean(getString(R.string.pref_show_only_passenger_trains), true);

        while (i.hasNext()) {
            Train t = i.next();

            boolean filterOut = filterPassengerTrains
                    && !(t.getTrainCategory().equals("Long-distance") || t.getTrainCategory().equals("Commuter"));

            if (oldTrains.contains(new Pair<>(t.getTrainNumber(), t.getDepartureDate())) || filterOut)
                i.remove();
        }
    }

    private boolean areTrainsBefore(List<Train> trains, Date compareDate,
            final String stationCode, final @TimeTableRowType String rowType) {
        // it is enough to just compare the scheduled times

        final EnumSet<Train.StationDataType> dataFields = EnumSet.of(Train.StationDataType.MOST_ACCURATE_TIME);

        for (int i = 0; i < trains.size(); i++) {
            Date scheduled = (Date) trains.get(i).getStationData(stationCode, rowType, dataFields, getActivity())
                  .get(Train.StationDataType.MOST_ACCURATE_TIME);

            if (scheduled.compareTo(compareDate) > 0) {
                Log.d(TAG, "Scheduled " + scheduled.toString() + " not before compareDate" + compareDate.toString());
                return false;
            }
        }

        return true;
    }

    /**
     * Builds a TrainDataHolder object out of a passed Train object.
     * <p>
     * Basically extracts various bits of data needed to show the train in the arriving/departing train card, and saves
     * the extracted info to a TrainDataHolder.
     *
     * @param train       The train object to get data from
     * @param stationCode The station short code of the station the train is stopping on. In english that means usually
     *                    the same station that the whole fragment shows details for.
     * @param rowType     {@link #TIMETABLE_ROW_TYPE_ARRIVAL} or {@link #TIMETABLE_ROW_TYPE_DEPARTURE}
     *
     * @return A TrainDataHolder containing all the data that we got from the Train
     */
    @NonNull
    private TrainDataHolder buildTrainDataHolder(Train train, final String stationCode,
            final @TimeTableRowType String rowType) {
        // get data for view
        SimpleArrayMap<Train.StationDataType, Object> stationData = train.getStationData(
                stationCode, rowType, EnumSet.of(
                        Train.StationDataType.FORMATTED_SCHEDULED_TIME,
                        Train.StationDataType.FORMATTED_REAL_TIME,
                        Train.StationDataType.IS_LATE,
                        Train.StationDataType.REAL_TIME_TYPE,
                        Train.StationDataType.MOST_ACCURATE_TIME), getActivity()
        );

        // make new data holder
        // todo try to implement object pools for reduced allocations
        TrainDataHolder h = new TrainDataHolder();

        h.rowType = rowType;

        h.title = train.getTrainTitle();

        String scheduledTime = (String) stationData.get(Train.StationDataType.FORMATTED_SCHEDULED_TIME);
        if (scheduledTime != null) {
            h.scheduledTime = scheduledTime;
        }

        String realTime = (String) stationData.get(Train.StationDataType.FORMATTED_REAL_TIME);
        if (realTime != null) {
            h.realTime = realTime;
        }

        h.isLate = (Boolean) stationData.get(Train.StationDataType.IS_LATE);
        h.realTimeType = (Train.TimeType) stationData.get(Train.StationDataType.REAL_TIME_TYPE);

        h.endStation = train.getEndStation(rowType, getActivity());

        h.departureDate = train.getDepartureDate();
        h.number = train.getTrainNumber();

        h.compareTime = ((Date) stationData.get(Train.StationDataType.MOST_ACCURATE_TIME)).getTime();

        return h;
    }

    @Override
    public void onTrainClicked(int trainNumber, String departureDate) {
        // pack the train number and departure date to a bundle, which we pass to the train detail activity
        Bundle args = new Bundle();
        args.putInt(TrainDetailFragment.ARG_TRAIN_NUMBER, trainNumber);
        args.putString(TrainDetailFragment.ARG_DEPARTURE_DATE, departureDate);

        // fire up TrainDetailActivity
        Intent i = new Intent(getActivity(), TrainDetailActivity.class);
        i.putExtra(TrainDetailActivity.EXTRA_ARGUMENT_BUNDLE, args);

        getActivity().startActivity(i);
    }

    @Override
    public void onLoadPreviousClicked() {
        if (mArriving != 0) {
            mArrived += 5;
        } else if (mDeparting != 0) {
            mDeparted += 5;
        }

        mAdapter.setIsHeaderLoading(true);
        mAdapter.notifyItemChanged(0);
        refresh(REFRESH_TYPE_PREVIOUS);
    }

    @Override
    public void onLoadNextClicked() {
        if (mArriving != 0) {
            mArriving += 5;
        } else if (mDeparting != 0) {
            mDeparting += 5;
        }

        mAdapter.setIsFooterLoading(true);
        mAdapter.notifyItemChanged(mAdapter.getItemCount() - 1);
        refresh(REFRESH_TYPE_NEXT);
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    /**
     * A helper class for saving various data for a arriving/departing train cell.
     */
    public static class TrainDataHolder {

        /**
         * The trains title (type + number, or commuter line id)
         */
        public String title;
        /**
         * The name of the station the train is going to / coming from
         */
        public String endStation;
        /**
         * The trains scheduled arrival/departure on this station
         */
        public String scheduledTime;
        /**
         * Millisecond timestamp of the most accurate arrival/departure, used when comparing dates
         */
        public long compareTime;
        /**
         * The estimated/actual departure/arrival time
         */
        public String realTime;
        /**
         * The trains number (used when user clicks)
         */
        public int number;
        /**
         * The trains departure date. Used for distinguishing train when user clicks cell
         */
        public String departureDate;
        /**
         * Whether the train is late or not
         */
        public boolean isLate;
        /**
         * The type of the saved real time (for example, actual departure or estimated arrival)
         */
        public Train.TimeType realTimeType;
        /**
         * {@link StationTrainListFragment#TIMETABLE_ROW_TYPE_DEPARTURE} or {@link StationTrainListFragment#TIMETABLE_ROW_TYPE_ARRIVAL}
         */
        public @TimeTableRowType String rowType;
    }

    /**
     * A simple divider that will not draw for the header and footer in this fragment's recyclerview
     */
    private class StationTrainListDividerDecoration extends InsetDividerItemDecoration {

        public StationTrainListDividerDecoration(Drawable divider) {
            super(divider, 0, false);
        }

        @Override
        protected boolean shouldDrawDivider(RecyclerView recyclerView, int index) {
            View v = recyclerView.getChildAt(index);
            int adapterPos = recyclerView.getChildAdapterPosition(v);

            if ((mAdapter.getHasHeader() && adapterPos == 0)
                    || (mAdapter.getHasFooter() && (adapterPos >= mAdapter.getItemCount() - 2))) {
                return false;
            } else {
                return super.shouldDrawDivider(recyclerView, index);
            }
        }
    }
}
