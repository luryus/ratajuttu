package tk.luryus.ratajuttu.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.HashSet;
import java.util.Set;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import tk.luryus.ratajuttu.R;
import tk.luryus.ratajuttu.provider.RataContract.StationEntry;
import tk.luryus.ratajuttu.ui.adapter.TabAdapter;
import tk.luryus.ratajuttu.ui.fragment.StationTrainListFragment;

public class StationDetailActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    private static final String TAG = "StationDetailActivity";

    /**
     * The content uri which the detail fetching is done with. Comes from main activity.
     */
    private Uri mDataUri;

    private String mStationCode;

    /**
     * Id for details cursor loader
     */
    private static final int STATION_DETAIL_LOADER = 0;

    /**
     * A cached status of whether the station is marked as favorite.
     */
    private boolean mIsFavorite;

    private Toolbar mToolbar;

    private MenuItem mFavoriteMenuItem;

    /**
     * The database columns that are fetched for this view
     */
    private static final String[] DETAIL_COLUMNS = {
            StationEntry.STATION_NAME,
            StationEntry.LATITUDE,
            StationEntry.LONGITUDE
    };

    /**
     * The column indexes. Update these if DETAIL_COLUMNS is updated.
     */
    private static final int COL_STATION_NAME = 0;
    private static final int COL_LATITUDE = 1;
    private static final int COL_LONGITUDE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // we should get a content provider uri as param for the station
        mDataUri = getIntent().getData();
        // from the uri get the station shortcode
        mStationCode = StationEntry.getStationShortCodeFromUri(mDataUri);
        mIsFavorite = getSharedPreferences("favorites", Context.MODE_PRIVATE)
                .getStringSet("station_favorites", new HashSet<>())
                .contains(mStationCode);

        // set up the activity layout
        setContentView(R.layout.activity_station_detail);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_station_detail);
        setSupportActionBar(mToolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }

        ViewPager pager = (ViewPager) findViewById(R.id.viewpager_station_detail);
        if (pager != null) {
            setupViewPager(pager);
            TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout_station_detail);
            tabLayout.setupWithViewPager(pager);
        }

        getLoaderManager().initLoader(STATION_DETAIL_LOADER, null, this);
    }

    private void setupViewPager(ViewPager pager) {
        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());

        adapter.addFragment(
                StationTrainListFragment.newInstance(
                        mStationCode, StationTrainListFragment.TIMETABLE_ROW_TYPE_DEPARTURE),
                getString(R.string.station_detail_departures_tab));
        adapter.addFragment(
                StationTrainListFragment.newInstance(
                        mStationCode, StationTrainListFragment.TIMETABLE_ROW_TYPE_ARRIVAL),
                getString(R.string.station_detail_arrivals_tab));

        pager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.station_detail, menu);
        mFavoriteMenuItem = menu.findItem(R.id.action_favorite);

        // when the menu is created, the favorite status should already be set
        if (mIsFavorite) {
            mFavoriteMenuItem.setIcon(R.drawable.ic_star_white);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_favorite:
                toggleFavoriteStation();
                return true;
            case R.id.action_settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // we handle getting the station name and other info with a loader
        if (mDataUri != null) {
            return new CursorLoader(
                    this,
                    mDataUri,
                    DETAIL_COLUMNS,
                    null,
                    null,
                    null
            );
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null && data.moveToFirst()) {
            ActionBar ab = getSupportActionBar();
            if (ab != null) {
                ab.setTitle(data.getString(COL_STATION_NAME));
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    /**
     * Toggles the current station's favorite status. Changes the preference in a background thread and then changes the
     * fab icon and mIsFavorite in the UI thread.
     */
    private void toggleFavoriteStation() {
        // change the preference in background thread
        Observable.create((Subscriber<? super Boolean> subscriber) -> {
                    SharedPreferences prefs = getSharedPreferences("favorites", Context.MODE_PRIVATE);
                    Set<String> currentFavs = prefs.getStringSet("station_favorites", null);
                    Set<String> newFavs;

                    if (currentFavs != null) {
                        newFavs = new HashSet<>(currentFavs.size() + 1);
                        newFavs.addAll(currentFavs);
                        if (mIsFavorite) {
                            newFavs.remove(mStationCode);
                        } else {
                            newFavs.add(mStationCode);
                        }
                    } else {
                        newFavs = new HashSet<>(1);
                        newFavs.add(mStationCode);
                    }

                    prefs.edit().putStringSet("station_favorites", newFavs).apply();

                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(!mIsFavorite);
                        subscriber.onCompleted();
                    }
                })
              .subscribeOn(Schedulers.newThread())
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe(isFavorite -> {
                  mIsFavorite = isFavorite;
                  if (mIsFavorite) {
                      mFavoriteMenuItem.setIcon(R.drawable.ic_star_white);
                  } else {
                      mFavoriteMenuItem.setIcon(R.drawable.ic_star_border_white);
                  }
              });
    }
}
