package tk.luryus.ratajuttu.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import tk.luryus.ratajuttu.R;
import tk.luryus.ratajuttu.rata.entity.Train;
import tk.luryus.ratajuttu.ui.fragment.StationTrainListFragment;
import tk.luryus.ratajuttu.ui.fragment.StationTrainListFragment.TrainDataHolder;

/**
 * Created by Lauri Koskela on 30/07/15.
 */
public class StationTrainListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_TRAIN_CELL = 2;
    private static final int TYPE_NO_TRAINS = 3;

    private ArrayList<TrainDataHolder> mContent;
    private boolean mHasHeader;
    private boolean mHasFooter;
    private boolean mHasNoTrains;

    private boolean mIsHeaderLoading, mIsFooterLoading;

    private OnTrainListClickListener mClickListener;

    public StationTrainListAdapter(OnTrainListClickListener listener) {
        mContent = new ArrayList<>(4);
        // by default, do not show the buttons
        mHasNoTrains = mHasFooter = mHasHeader = false;
        mIsFooterLoading = mIsHeaderLoading = false;
        mClickListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER || viewType == TYPE_FOOTER) {
            View headerFooterView =
                    LayoutInflater.from(parent.getContext())
                                  .inflate(R.layout.list_item_station_trains_headerfooter, parent, false);
            return new HeaderFooterViewHolder(headerFooterView);
        } else if (viewType == TYPE_TRAIN_CELL) {
            View trainCellView = LayoutInflater.from(parent.getContext())
                                               .inflate(R.layout.train_cell_station, parent, false);
            return new TrainViewHolder(trainCellView);
        } else if (viewType == TYPE_NO_TRAINS) {
            View noTrainsCell = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.train_cell_empty, parent, false);
            return new NoTrainsViewHolder(noTrainsCell);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderFooterViewHolder) {
            if (position == 0) {
                bindHeaderViewHolder((HeaderFooterViewHolder) holder);
            } else {
                bindFooterViewHolder((HeaderFooterViewHolder) holder);
            }
        } else if (holder instanceof TrainViewHolder) {
            bindTrainViewHolder((TrainViewHolder) holder, position);
        } else if (holder instanceof NoTrainsViewHolder) {
            // at this point we do not have to do anything
        }
    }

    private void bindTrainViewHolder(TrainViewHolder holder, int position) {
        if (mHasHeader) position--;

        TrainDataHolder data = mContent.get(position);
        // set train title and scheduled time (these are pretty straightforward)
        holder.getTextViewTrainTitle().setText(data.title);
        holder.getTextViewScheduledTime().setText(data.scheduledTime);

        // also show the name of the station the train is going to / coming from
        holder.getTextViewEndStationName().setText(data.endStation);

        Context ctx = holder.getTextViewRealTime().getContext();

        if (!data.isLate && data.realTimeType != Train.TimeType.ACTUAL) {
            holder.getTextViewRealTime().setText(R.string.train_on_time);
        } else if (data.realTimeType == Train.TimeType.ACTUAL) {
            // we have actual arrival/departure time
            if (data.rowType.equals(StationTrainListFragment.TIMETABLE_ROW_TYPE_ARRIVAL)) {
                holder.getTextViewRealTime()
                      .setText(ctx.getText(R.string.train_actual_arrival) + " " + data.realTime);
            } else {
                holder.getTextViewRealTime()
                      .setText(ctx.getText(R.string.train_actual_departure) + " " + data.realTime);
            }
        } else if (data.realTimeType == Train.TimeType.ESTIMATED) {
            // we have just an estimate of the future arrival/departure
            if (data.rowType.equals(StationTrainListFragment.TIMETABLE_ROW_TYPE_ARRIVAL)) {
                holder.getTextViewRealTime()
                      .setText(ctx.getText(R.string.train_estimated_arrival) + " " + data.realTime);
            } else {
                holder.getTextViewRealTime()
                      .setText(ctx.getText(R.string.train_estimated_departure) + " " + data.realTime);
            }
        }

        // if the train is late, make the real time text red.
        if (data.isLate) {
            holder.getTextViewRealTime().setTextColor(ctx.getResources().getColor(R.color.train_late));
        } else {
            holder.getTextViewRealTime()
                  .setTextColor(ctx.getResources().getColor(R.color.secondary_text_default_material_light));
        }

        // lastly, add some backend info to the views as tags. These are used when user clicks the train cells
        holder.setTrainNumber(data.number);
        holder.setDepartureDate(data.departureDate);
    }

    private void bindHeaderViewHolder(HeaderFooterViewHolder holder) {
        holder.setType(TYPE_HEADER);
        holder.getLoadMoreButton().setText(R.string.station_detail_load_previous);

        if (mIsHeaderLoading) {
            holder.mProgressBar.setEnabled(true);
            holder.mProgressBar.setVisibility(View.VISIBLE);
            holder.mLoadMoreButton.setVisibility(View.GONE);
        } else {
            holder.mProgressBar.setEnabled(false);
            holder.mProgressBar.setVisibility(View.GONE);
            holder.mLoadMoreButton.setVisibility(View.VISIBLE);
        }
    }

    private void bindFooterViewHolder(HeaderFooterViewHolder holder) {
        holder.setType(TYPE_FOOTER);
        holder.getLoadMoreButton().setText(R.string.station_detail_load_next);

        if (mIsFooterLoading) {
            holder.mProgressBar.setEnabled(true);
            holder.mProgressBar.setVisibility(View.VISIBLE);
            holder.mLoadMoreButton.setVisibility(View.GONE);
        } else {
            holder.mProgressBar.setEnabled(false);
            holder.mProgressBar.setVisibility(View.GONE);
            holder.mLoadMoreButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return (mHasNoTrains ? 1 : mContent.size()) + (mHasFooter ? 1 : 0) + (mHasHeader ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (mHasHeader && position == 0) {
            return TYPE_HEADER;
        } else if (mHasFooter && position == getItemCount() - 1) {
            return TYPE_FOOTER;
        } else if (mHasNoTrains && position == (mHasHeader ? 1 : 0)) {
            return TYPE_NO_TRAINS;
        } else {
            return TYPE_TRAIN_CELL;
        }
    }

    public void setHasNoTrains(boolean hasNoTrains) {
        mHasNoTrains = hasNoTrains;
    }

    public ArrayList<TrainDataHolder> getContent() {
        return mContent;
    }

    public void setContent(ArrayList<TrainDataHolder> content) {
        mContent = content;
    }

    public boolean getHasHeader() {
        return mHasHeader;
    }

    public void setHasHeader(boolean hasHeader) {
        mHasHeader = hasHeader;
    }

    public boolean getHasFooter() {
        return mHasFooter;
    }

    public void setHasFooter(boolean hasFooter) {
        mHasFooter = hasFooter;
    }

    public void setIsHeaderLoading(boolean isHeaderLoading) {
        mIsHeaderLoading = isHeaderLoading;
    }

    public void setIsFooterLoading(boolean isFooterLoading) {
        mIsFooterLoading = isFooterLoading;
    }

    private class HeaderFooterViewHolder extends RecyclerView.ViewHolder {

        private Button mLoadMoreButton;
        private int mType;
        private ProgressBar mProgressBar;

        public HeaderFooterViewHolder(View itemView) {
            super(itemView);
            mLoadMoreButton = (Button) itemView.findViewById(R.id.button_load_more);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar_load_more);

            mLoadMoreButton.setOnClickListener(v -> {
                if (mType == TYPE_HEADER) {
                    mClickListener.onLoadPreviousClicked();
                } else {
                    mClickListener.onLoadNextClicked();
                }
            });
        }

        public Button getLoadMoreButton() {
            return mLoadMoreButton;
        }

        public int getType() {
            return mType;
        }

        public void setType(int type) {
            mType = type;
        }
    }

    private class TrainViewHolder extends RecyclerView.ViewHolder {

        private FrameLayout mMainView;

        private TextView mTextViewEndStationName;
        private TextView mTextViewScheduledTime;
        private TextView mTextViewTrainTitle;
        private TextView mTextViewRealTime;

        private String mDepartureDate;
        private int mTrainNumber;

        public TrainViewHolder(View itemView) {
            super(itemView);
            mMainView = (FrameLayout) itemView;
            mTextViewEndStationName = (TextView) itemView.findViewById(R.id.textview_endstation_name);
            mTextViewScheduledTime = (TextView) itemView.findViewById(R.id.textview_scheduled_time);
            mTextViewTrainTitle = (TextView) itemView.findViewById(R.id.textview_train_title);
            mTextViewRealTime = (TextView) itemView.findViewById(R.id.textview_real_time);

            View mainContent = itemView.findViewById(R.id.linearlayout_station_train_cell_content);
            mainContent.setOnClickListener(v -> mClickListener.onTrainClicked(mTrainNumber, mDepartureDate));
        }

        public FrameLayout getMainView() {
            return mMainView;
        }

        public TextView getTextViewEndStationName() {
            return mTextViewEndStationName;
        }

        public TextView getTextViewScheduledTime() {
            return mTextViewScheduledTime;
        }

        public TextView getTextViewTrainTitle() {
            return mTextViewTrainTitle;
        }

        public TextView getTextViewRealTime() {
            return mTextViewRealTime;
        }

        public void setDepartureDate(String departureDate) {
            mDepartureDate = departureDate;
        }

        public void setTrainNumber(int trainNumber) {
            mTrainNumber = trainNumber;
        }
    }

    private class NoTrainsViewHolder extends RecyclerView.ViewHolder {

        public NoTrainsViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnTrainListClickListener {
        void onTrainClicked(int trainNumber, String departureDate);
        void onLoadPreviousClicked();
        void onLoadNextClicked();
    }
}
