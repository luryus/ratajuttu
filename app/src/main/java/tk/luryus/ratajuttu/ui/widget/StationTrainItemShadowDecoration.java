package tk.luryus.ratajuttu.ui.widget;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;


/**
 * Copied code from com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator
 *
 * This is a modified version of class mentioned above. Canvas clipping has been removed, because it wasn't working
 * with ViewPager (with it's parameters at least). Seems to work fine just without it.
 */
public class StationTrainItemShadowDecoration extends RecyclerView.ItemDecoration {

    private final NinePatchDrawable mShadowDrawable;
    private final Rect mShadowPadding = new Rect();

    public StationTrainItemShadowDecoration(NinePatchDrawable shadow) {
        mShadowDrawable = shadow;
        mShadowDrawable.getPadding(mShadowPadding);
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        final int childCount = parent.getChildCount();

        if (childCount == 0) {
            return;
        }

        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);

            if (!shouldDrawDropShadow(child)) {
                continue;
            }

            final int tx = (int) (ViewCompat.getTranslationX(child) + 0.5f);
            final int ty = (int) (ViewCompat.getTranslationY(child) + 0.5f);

            final int left = child.getLeft() - mShadowPadding.left;
            final int right = child.getRight() + mShadowPadding.right;
            final int top = child.getTop() - mShadowPadding.top;
            final int bottom = child.getBottom() + mShadowPadding.bottom;

            mShadowDrawable.setBounds(left + tx, top + ty, right + tx, bottom + ty);
            mShadowDrawable.draw(c);
        }
    }

    private static boolean shouldDrawDropShadow(View child) {
        if (child.getVisibility() != View.VISIBLE) {
            return false;
        }
        if (ViewCompat.getAlpha(child) != 1.0f) {
            return false;
        }

        Drawable background = child.getBackground();
        if (background == null) {
            return false;
        }

        if (background instanceof ColorDrawable) {
            //noinspection RedundantCast
            if (((ColorDrawable) background).getAlpha() == 0) {
                return false;
            }
        }

        return true;
    }
}
