package tk.luryus.ratajuttu.ui.widget;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * A simple RecyclerView list divider decoration, which also allows determining an inset, so that the divider does
 * not start from the left edge.
 *
 * Only works for vertical lists.
 */
public class InsetDividerItemDecoration extends RecyclerView.ItemDecoration {

    /**
     * The divider drawable
     */
    private Drawable mDivider;
    /**
     * The amount of inset
     */
    private int mInset;
    /**
     * If the last divider should be drawn.
     * TODO: at the moment this does not probably work. This does not look for adapter's last item, but rather uses
     * the last item currently in the recycler view
     */
    private boolean mDrawLastDivider;

    /**
     * @param divider The divider drawable
     * @param inset The width of the inset
     * @param drawLastDivider If the last divider in the list should be drawn
     */
    public InsetDividerItemDecoration(Drawable divider, int inset, boolean drawLastDivider) {
        mDivider = divider;
        mInset = inset;
        mDrawLastDivider = drawLastDivider;
    }

    /**
     * A method which is called for every view in the recycler view to determine if the divider will be drawn for
     * them. This can be overriden for custom behavior.
     *
     * @param recyclerView The recyclerView we are drawing dividers for
     * @param index The index of the item (in RecyclerView, not adapter)
     *
     * @return if the divider should be drawn or not
     */
    protected boolean shouldDrawDivider(RecyclerView recyclerView, int index) {
        return mDrawLastDivider || index != (recyclerView.getChildCount() - 1);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        // if no divider specified, do not draw anything
        if (mDivider == null) {
            super.onDrawOver(c, parent, state);
            return;
        }

        // the left edge of the divider is after the left padding and inset
        final int left = parent.getPaddingLeft() + mInset;
        final int right = parent.getWidth() - parent.getPaddingRight();
        final int height = mDivider.getIntrinsicHeight();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (shouldDrawDivider(parent, i)) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                // translation for the recyclerview animations (so that the dividers will not be left behind)
                final int ty = (int) (ViewCompat.getTranslationY(child) + 0.5f);
                final int top = ty + child.getBottom() + params.bottomMargin - height;
                final int bottom = top + height;

                mDivider.setBounds(left, top, right, bottom);
                // alpha, because when animating new items in, their alpha is 0 first
                mDivider.setAlpha((int) (255 * child.getAlpha()));
                mDivider.draw(c);
            }
        }
    }
}
