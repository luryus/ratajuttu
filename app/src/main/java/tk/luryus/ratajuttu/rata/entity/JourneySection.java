package tk.luryus.ratajuttu.rata.entity;

import java.util.List;

public class JourneySection extends RataEntity {
    private CompositionTimeTableRow beginTimeTableRow;
    private CompositionTimeTableRow endTimeTableRow;
    private List<Locomotive> locomotives;
    private List<Wagon> wagons;
    private int totalLength;
    private int maximumSpeed;

    public CompositionTimeTableRow getBeginTimeTableRow() {
        return beginTimeTableRow;
    }

    public void setBeginTimeTableRow(CompositionTimeTableRow beginTimeTableRow) {
        this.beginTimeTableRow = beginTimeTableRow;
    }

    public CompositionTimeTableRow getEndTimeTableRow() {
        return endTimeTableRow;
    }

    public void setEndTimeTableRow(CompositionTimeTableRow endTimeTableRow) {
        this.endTimeTableRow = endTimeTableRow;
    }

    public List<Locomotive> getLocomotives() {
        return locomotives;
    }

    public void setLocomotives(List<Locomotive> locomotives) {
        this.locomotives = locomotives;
    }

    public List<Wagon> getWagons() {
        return wagons;
    }

    public void setWagons(List<Wagon> wagons) {
        this.wagons = wagons;
    }

    public int getTotalLength() {
        return totalLength;
    }

    public void setTotalLength(int totalLength) {
        this.totalLength = totalLength;
    }

    public int getMaximumSpeed() {
        return maximumSpeed;
    }

    public void setMaximumSpeed(int maximumSpeed) {
        this.maximumSpeed = maximumSpeed;
    }
}
