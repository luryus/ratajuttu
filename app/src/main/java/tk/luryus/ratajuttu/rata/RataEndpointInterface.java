package tk.luryus.ratajuttu.rata;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;
import tk.luryus.ratajuttu.rata.entity.Operator;
import tk.luryus.ratajuttu.rata.entity.Station;
import tk.luryus.ratajuttu.rata.entity.Train;

/**
 * Created by lauri.koskela on 6/22/15.
 */
public interface RataEndpointInterface {

    @GET("/metadata/station")
    List<Station> getStations();

    @GET("/metadata/operator")
    List<Operator> getOperators();

/*    @GET("/metadata/cause_category_code")
    void getCauseCategories(Callback<List<CauseCategory>> cb);

    @GET("/metadata/detailed_cause_category_code")
    void getDetailedCauseCategories(Callback<List<DetailedCauseCategory>> cb);

    @GET("/metadata/train_type")
    void getTrainTypes(Callback<List<TrainType>> cb);*/

    // --- Live trains ---

    @GET("/live-trains")
    Observable<List<Train>> getLiveTrainsForStation(
            @Query("station") String station,
            @Query("arrived_trains") int arrivedTrains,
            @Query("arriving_trains") int arrivingTrains,
            @Query("departed_trains") int departedTrains,
            @Query("departing_trains") int departingTrains);

    // API always returns an array for /live-trains/, even for a single or no trains
    @GET("/live-trains/{train}")
    Observable<List<Train>> getLiveTrain(@Path("train") int trainNumber, @Query("departure_date") String departureDate);

    @GET("/live-trains")
    Observable<List<Train>> getLiveTrains();
}
