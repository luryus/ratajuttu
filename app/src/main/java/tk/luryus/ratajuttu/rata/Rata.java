package tk.luryus.ratajuttu.rata;

import android.support.v4.util.Pools.SynchronizedPool;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class Rata {

    private static final String API_BASE_URL = "http://rata.digitraffic.fi/api/v1";

    private static final RataEndpointInterface sApiService;

    static {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();

        RestAdapter restAdapter;
        restAdapter = new RestAdapter.Builder().setEndpoint(API_BASE_URL)
                .setConverter(new GsonConverter(gson))
                .build();
        sApiService = restAdapter.create(RataEndpointInterface.class);
    }

    public static RataEndpointInterface getApiService() {
        return sApiService;
    }

    public static class DateDeserializer implements JsonDeserializer<Date> {

        private static final String[] DATE_FORMATS = {
                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                "yyyy-MM-dd"
        };

        private final ArrayList<SynchronizedPool<SimpleDateFormat>> mFormatPools;

        public DateDeserializer() {
            mFormatPools = new ArrayList<>(DATE_FORMATS.length);
            mFormatPools.addAll(Collections.nCopies(DATE_FORMATS.length,
                    new SynchronizedPool<>(Runtime.getRuntime().availableProcessors())));
        }

        private SimpleDateFormat obtainFormat(int index) {
            SimpleDateFormat instance = mFormatPools.get(index).acquire();
            if (instance == null) {
                instance = new SimpleDateFormat(DATE_FORMATS[index], Locale.ENGLISH);
                instance.setTimeZone(TimeZone.getTimeZone("UTC"));
            }
            return instance;
        }

        private void recycleFormat(SimpleDateFormat format, int index) {
            mFormatPools.get(index).release(format);
        }

        @Override
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            for (int i = 0; i < DATE_FORMATS.length; i++) {
                SimpleDateFormat dateFormat = obtainFormat(i);
                try {
                    return dateFormat.parse(json.getAsString());
                } catch (ParseException ignored) {
                } finally {
                    recycleFormat(dateFormat, i);
                }
            }

            // throw error if we make it here.
            throw new JsonParseException("Cannot parse date: " + json.getAsString());
        }
    }

}
