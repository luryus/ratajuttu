package tk.luryus.ratajuttu.rata.entity;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.util.SimpleArrayMap;

import java.text.DateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.TimeZone;

import tk.luryus.ratajuttu.provider.RataContract;
import tk.luryus.ratajuttu.ui.fragment.StationTrainListFragment;

public class Train extends RataEntity {

    private int trainNumber;
    // departureDate is only used for fetching more information, so it's better to keep it as String
    private String departureDate;
    private int operatorUICCode;
    private String operatorShortCode;
    private String trainType;
    private String trainCategory;
    private String commuterLineID;
    private boolean runningCurrently;
    private boolean cancelled;
    private long version;
    private TimeTableRow[] timeTableRows;

    public enum StationDataType {
        TRACK, FORMATTED_SCHEDULED_TIME, FORMATTED_REAL_TIME, SCHEDULED_TIME, IS_LATE, REAL_TIME_TYPE,
        MOST_ACCURATE_TIME, MOST_ACCURATE_TIME_TYPE
    }

    public enum TimeType {
        ESTIMATED, ACTUAL, SCHEDULED, INVALID
    }

    public static final String TRAIN_CATEGORY_LONG_DISTANCE = "Long-distance";
    public static final String TRAIN_CATEGORY_COMMUTER = "Commuter";
    public static final String TRAIN_CATEGORY_CARGO = "Cargo";
    public static final String TRAIN_CATEGORY_LOCOMOTIVE = "Locomotive";
    public static final String TRAIN_CATEGORY_SHUNTING = "Shunting";
    public static final String TRAIN_CATEGORY_ON_TRACK_MACHINES = "On-track machines";

    public int getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public int getOperatorUICCode() {
        return operatorUICCode;
    }

    public void setOperatorUICCode(int operatorUICCode) {
        this.operatorUICCode = operatorUICCode;
    }

    public String getOperatorShortCode() {
        return operatorShortCode;
    }

    public void setOperatorShortCode(String operatorShortCode) {
        this.operatorShortCode = operatorShortCode;
    }

    public String getTrainType() {
        return trainType;
    }

    public void setTrainType(String trainType) {
        this.trainType = trainType;
    }

    public String getTrainCategory() {
        return trainCategory;
    }

    public void setTrainCategory(String trainCategory) {
        this.trainCategory = trainCategory;
    }

    public String getCommuterLineID() {
        return commuterLineID;
    }

    public void setCommuterLineID(String commuterLineID) {
        this.commuterLineID = commuterLineID;
    }

    public boolean isRunningCurrently() {
        return runningCurrently;
    }

    public void setRunningCurrently(boolean runningCurrently) {
        this.runningCurrently = runningCurrently;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public TimeTableRow[] getTimeTableRows() {
        return timeTableRows;
    }

    public void setTimeTableRows(TimeTableRow[] timeTableRows) {
        this.timeTableRows = timeTableRows;
    }


    public SimpleArrayMap<StationDataType, Object> getStationData(String stationCode,
                                                           String rowType,
                                                           EnumSet<StationDataType> dataKeys,
                                                           Context context) {
        SimpleArrayMap<StationDataType, Object> returnMap = new SimpleArrayMap<>(dataKeys.size());

        TimeTableRow ttr;
        if (!dataKeys.isEmpty()) {
            ttr = findStationTimeTableRow(stationCode, rowType);
        } else {
            return returnMap;
        }

        if (ttr != null) {
            DateFormat df = android.text.format.DateFormat.getTimeFormat(context);
            df.setTimeZone(TimeZone.getDefault());

            if (dataKeys.contains(StationDataType.TRACK)) {
                returnMap.put(StationDataType.TRACK, ttr.getCommercialTrack());
            }

            if (dataKeys.contains(StationDataType.FORMATTED_SCHEDULED_TIME)) {
                returnMap.put(StationDataType.FORMATTED_SCHEDULED_TIME,
                        df.format(ttr.getScheduledTime())
                );
            }

            if (dataKeys.contains(StationDataType.FORMATTED_REAL_TIME)) {
                String realFormatted;
                if (ttr.getActualTime() != null)
                    realFormatted = df.format(ttr.getActualTime());
                else if (ttr.getLiveEstimateTime() != null)
                    realFormatted = df.format(ttr.getLiveEstimateTime());
                else
                    realFormatted = null;

                returnMap.put(StationDataType.FORMATTED_REAL_TIME, realFormatted);
            }

            if (dataKeys.contains(StationDataType.SCHEDULED_TIME)) {
                returnMap.put(StationDataType.SCHEDULED_TIME, ttr.getScheduledTime());
            }

            if (dataKeys.contains(StationDataType.IS_LATE)) {
                returnMap.put(StationDataType.IS_LATE, ttr.getDifferenceInMinutes() > 0);
            }

            if (dataKeys.contains(StationDataType.REAL_TIME_TYPE)) {
                if (ttr.getActualTime() != null)
                    returnMap.put(StationDataType.REAL_TIME_TYPE, TimeType.ACTUAL);
                else if (ttr.getLiveEstimateTime() != null)
                    returnMap.put(StationDataType.REAL_TIME_TYPE, TimeType.ESTIMATED);
                else
                    returnMap.put(StationDataType.REAL_TIME_TYPE, TimeType.INVALID);
            }

            if (dataKeys.contains(StationDataType.MOST_ACCURATE_TIME)
                    || dataKeys.contains(StationDataType.MOST_ACCURATE_TIME_TYPE)) {

                if (ttr.getActualTime() != null) {
                    returnMap.put(StationDataType.MOST_ACCURATE_TIME, ttr.getActualTime());
                } else if (ttr.getLiveEstimateTime() != null) {
                    returnMap.put(StationDataType.MOST_ACCURATE_TIME, ttr.getLiveEstimateTime());
                } else if (ttr.getScheduledTime() != null) {
                    returnMap.put(StationDataType.MOST_ACCURATE_TIME, ttr.getScheduledTime());
                } else { // this pretty much never should happen
                    returnMap.put(StationDataType.MOST_ACCURATE_TIME, null);
                }
            }
        }

        return returnMap;
    }

    public String getEndStation(String rowType, Context context) {
        int index = 0;
        if (rowType.equals(StationTrainListFragment.TIMETABLE_ROW_TYPE_DEPARTURE)) {
            index = timeTableRows.length - 1;
        }

        String[] projection = {
                RataContract.StationEntry.STATION_NAME
        };

        String selectionClause = RataContract.StationEntry.STATION_SHORT_CODE + " = ?";

        Cursor c = context.getContentResolver().query(
                RataContract.StationEntry.CONTENT_URI,
                projection,
                selectionClause,
                new String[] { timeTableRows[index].getStationShortCode() },
                null
        );

        String retStr = null;
        if (c != null) {
            if (c.moveToFirst()) {
                retStr = c.getString(0);
                c.close();
            } else {
                c.close();
                return null;
            }
        }

        return retStr;
    }

    public String getDestinationShortCode() {
        return this.timeTableRows[timeTableRows.length - 1].getStationShortCode();
    }

    public String getOriginShortCode() {
        return this.timeTableRows[0].getStationShortCode();
    }

    public Date getScheduledOriginDeparture() {
        return timeTableRows[0].getScheduledTime();
    }

    public Date getScheduledDestinationArrival() {
        return timeTableRows[timeTableRows.length - 1].getScheduledTime();
    }

    private TimeTableRow findStationTimeTableRow(String stationCode, String rowType) {
        for (TimeTableRow ttr : timeTableRows) {
            if (ttr.getStationShortCode().equals(stationCode)
                    && ttr.getType().equals(rowType)){
                return ttr;
            }
        }

        return null;
    }

    public String getTrainTitle() {
        if (commuterLineID != null) {
            return commuterLineID;
        } else {
            return trainType + trainNumber;
        }
    }
}
