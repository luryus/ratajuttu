package tk.luryus.ratajuttu.rata.entity;

import java.util.List;

import tk.luryus.ratajuttu.rata.Rata;

public class Operator extends RataEntity {
    private String operatorName;
    private String operatorShortCode;
    private int operatorUICCode;
    private List<TrainNumberRange> trainNumbers;

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getOperatorShortCode() {
        return operatorShortCode;
    }

    public void setOperatorShortCode(String operatorShortCode) {
        this.operatorShortCode = operatorShortCode;
    }

    public int getOperatorUICCode() {
        return operatorUICCode;
    }

    public void setOperatorUICCode(int operatorUICCode) {
        this.operatorUICCode = operatorUICCode;
    }

    public List<TrainNumberRange> getTrainNumbers() {
        return trainNumbers;
    }

    public void setTrainNumbers(List<TrainNumberRange> trainNumbers) {
        this.trainNumbers = trainNumbers;
    }

}
