package tk.luryus.ratajuttu.rata.entity;


public class DetailedCauseCategory extends RataEntity {
    private String detailedCategoryCode;
    private String detailedCategoryName;

    public String getDetailedCategoryCode() {
        return detailedCategoryCode;
    }

    public void setDetailedCategoryCode(String detailedCategoryCode) {
        this.detailedCategoryCode = detailedCategoryCode;
    }

    public String getDetailedCategoryName() {
        return detailedCategoryName;
    }

    public void setDetailedCategoryName(String detailedCategoryName) {
        this.detailedCategoryName = detailedCategoryName;
    }
}
