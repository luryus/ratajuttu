package tk.luryus.ratajuttu.rata.entity;

public class TrainCategory extends RataEntity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
