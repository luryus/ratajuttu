package tk.luryus.ratajuttu.rata.entity;

public class Locomotive extends RataEntity {
    private int location;
    private String locomotiveType;
    private String powerType;

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public String getLocomotiveType() {
        return locomotiveType;
    }

    public void setLocomotiveType(String locomotiveType) {
        this.locomotiveType = locomotiveType;
    }

    public String getPowerType() {
        return powerType;
    }

    public void setPowerType(String powerType) {
        this.powerType = powerType;
    }
}
