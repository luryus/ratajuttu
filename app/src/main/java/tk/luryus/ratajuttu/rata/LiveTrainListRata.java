package tk.luryus.ratajuttu.rata;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import tk.luryus.ratajuttu.rata.entity.TimeTableRow;

/**
 * Created by Lauri Koskela on 13/09/15.
 */
public class LiveTrainListRata {

    private static final String API_BASE_URL = "http://rata.digitraffic.fi/api/v1";

    public static RataEndpointInterface getApiService() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new Rata.DateDeserializer())
                .registerTypeAdapter(TimeTableRow[].class, new TimeTableRowArrayDeserializer())
                .create();

        RestAdapter restAdapter;
        restAdapter = new RestAdapter.Builder().setEndpoint(API_BASE_URL)
                                               .setConverter(new GsonConverter(gson))
                                               .build();
        return restAdapter.create(RataEndpointInterface.class);
    }

    public static class TimeTableRowArrayDeserializer implements JsonDeserializer<TimeTableRow[]> {

        @Override
        public TimeTableRow[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            JsonArray ttrArray = json.getAsJsonArray();
            if (ttrArray.size() > 2) {
                int removeCount = ttrArray.size() - 2;
                while (removeCount-- > 2) {
                    ttrArray.remove(1);
                }
            }

            TimeTableRow[] retArray = new TimeTableRow[ttrArray.size()];
            for (int i = 0; i < retArray.length; i++) {
                retArray[i] = context.deserialize(ttrArray.get(i), TimeTableRow.class);
            }

            return retArray;
        }
    }
}
