package tk.luryus.ratajuttu.rata.entity;

public class TrainType extends RataEntity {

    private String name;
    private TrainCategory trainCategory;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TrainCategory getTrainCategory() {
        return trainCategory;
    }

    public void setTrainCategory(TrainCategory trainCategory) {
        this.trainCategory = trainCategory;
    }
}
