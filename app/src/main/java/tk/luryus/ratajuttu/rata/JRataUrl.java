package tk.luryus.ratajuttu.rata;

/**
 * Created by lauri.koskela on 6/15/15.
 */
public class JRataUrl {

    public static final String API_BASE_URL = "http://rata.digitraffic.fi/api/v1";

    public static final String METADATA_STATION = "/metadata/station";
    public static final String METADATA_OPERATOR = "/metadata/operator";
    public static final String METADATA_CAUSE_CATEGORY_CODE = "/metadata/cause_category_code";
    public static final String METADATA_DETAILED_CAUSE_CATEGORY_CODE = "/metadata/detailed_cause_category_code";
    public static final String METADATA_TRAIN_TYPE = "/metadata/train_type";

    public static final String LIVE_TRAINS_ALL = "/live-trains";
    public static final String LIVE_TRAIN_SPECIFIC = "/live-trains/{train_number}";

    public static final String SCHEDULES = "/schedules";

    public static final String HISTORY_ALL_TRAINS = "/history";
    public static final String HISTORY_SPECIFIC_TRAIN = "/history/{train_number}";

    public static final String COMPOSITIONS_ALL_TRAINS = "/compositions";
    public static final String COMPOSITIONS_SPECIFIC_TRAIN = "/composition/{train_number}";

    private String pathUrl;
    private Argument[] args = {};

    private JRataUrl(String pathUrl) {
        this.pathUrl = pathUrl;
    }

    public static JRataUrl create(String locationUrl) {
        return new JRataUrl(locationUrl);
    }

    public static JRataUrl createWithParams(String locationUrl, Argument[] args) {
        JRataUrl url = new JRataUrl(locationUrl);
        url.args = args;
        return url;
    }

    public String asString() {
        StringBuilder stringBuilder = new StringBuilder(API_BASE_URL);
        stringBuilder.append(pathUrl);
        for (int i = 0; i < args.length; i++) {
            stringBuilder.append((i == 0) ? '?' : '&');
            stringBuilder.append(args[i].getName());
            stringBuilder.append('=');
            stringBuilder.append(args[i].getValue());
        }
        return stringBuilder.toString();
    }

    public static class Argument {
        private String name;
        private String value;

        public Argument(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
