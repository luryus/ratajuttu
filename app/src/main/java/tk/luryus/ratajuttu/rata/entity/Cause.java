package tk.luryus.ratajuttu.rata.entity;

public class Cause extends RataEntity {
    private String categoryCode;
    private String detailedCategoryCode;

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getDetailedCategoryCode() {
        return detailedCategoryCode;
    }

    public void setDetailedCategoryCode(String detailedCategoryCode) {
        this.detailedCategoryCode = detailedCategoryCode;
    }
}
